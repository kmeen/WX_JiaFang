<?php

return array(
    //默认错误跳转对应的模板文件
    'TMPL_ACTION_ERROR' => 'Public/dispatch_jump',
    //默认成功跳转对应的模板文件
    'TMPL_ACTION_SUCCESS' => 'Public/dispatch_jump',
    
    'TMPL_PARSE_STRING' => array(
        '__PUBLIC__' => __ROOT__.'/Public',
        '__IMG__'=>__ROOT__ . '/Application/' . MODULE_NAME . '/View/' . $config['DEFAULT_THEME'] . '/Public/img',
        '__CSS__'=>__ROOT__ . '/Application/' . MODULE_NAME . '/View/' . $config['DEFAULT_THEME'] . '/Public/css',
        '__JS__'=> __ROOT__ . '/Application/' . MODULE_NAME . '/View/' . $config['DEFAULT_THEME'] . '/Public/js',
    ),
    
);
