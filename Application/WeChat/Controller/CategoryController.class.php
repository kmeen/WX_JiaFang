<?php

namespace WeChat\Controller;

class CategoryController extends BaseController {

    protected function _initialize() {

        parent::_initialize();
    }

    /*
     * 根据分类ID取出分类下的所有上架产品
     */

    public function index($id = 0) {

        $map['category'] = array('eq', $id);

        $map['status'] = 1; // 0、下架 1、上架
        #默认以价格由低到高排序
        $category_goods = D('Goods')->where($map)->order('price')->select();

        $model = D('category');

        $category = $model->find($id);

        $this->assign("goods", $category_goods);

        $this->assign("title", $category['title']);

        $this->display();
    }

    /*
     * 首页顶部分类模块产品
     */

    public function special_index() {

        $sp_id = I('param.sp_id');

        $map['special_category'] = $sp_id;

        $map['status'] = 1;

        $sp_goods = D('Goods')->where($map)->order('price')->select();

        $this->assign("goods", $sp_goods);

        $special_cateogry_array = C('Category_icon');

        $this->assign("title", $special_cateogry_array[$sp_id]);

        $this->display('index');
    }

}
