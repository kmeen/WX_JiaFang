<?php

namespace WeChat\Controller;

use Common\Model\MemberModel;

class GoodsController extends BaseController {
   
    private $uid = 0;

    protected function _initialize() {
        parent::_initialize();

     $this->uid = $this->login();
    // $this->uid = 395;//===delete
    }
 
    public function index() {
        redirect('/index.php?s=/WeChat/Index/index');     
    }

    
    /*
     * 商品详情
     */
    public function detail($id) {

        $goods = D('Goods')->find($id);              
        $user_model = new MemberModel();

        if (empty($goods)) {            
            $this->error('浏览商品不存在！');
        }

        //处理商品详情多图 
        if (!empty($goods['cover_multi'])) {            
            $goods['cover_multi'] = explode(',', $goods['cover_multi']);
        }
        
        $goods['price'] = $user_model->getPrice($goods['price'], $this->uid);
        
         if (!isset($_SESSION['user_id'])) {
            
            $_SESSION['user_id'] = $this->uid;
        }

        $this->assign("goods", $goods);        
        $this->assign("meta_title", $goods['title']);        
        $this->display();
    }

    /*
     * 搜索商品
     */

    public function search() {

        $keyword = I('post.keyword');  //获取关键字

        $map['title'] = array('like', '%' . $keyword . '%');  //模糊查询

        $search_goods = M('Goods')->where($map)->select();

        $this->assign('data', $search_goods);

        $this->assign('keyword', $keyword);

        $this->display();
    }

}
