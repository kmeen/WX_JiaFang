<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WeChat\Controller;

/**
 * 会员转赠积分表
 * Description of MemberSendscoreController
 *
 * @author xingji
 */
class MemberSendscoreController extends BaseController {
    
    private $uid;

    protected function _initialize() {
        
        parent::_initialize();

        //$this->uid =  $this->login();
        $this->uid = 6056;
    }

    /*
     * 积分转赠表
     */

    public function send_score() {
        

        if (IS_POST) {
            //dump(I('post.'));exit;
            $userid = I('post.userid');

            $receiveid = I('post.receiveid');

            $member = M('Member');

            $mebscore = M('MemberSendscore');

            $total = I('post.total');

            $balance = I('post.balance');

            $rel = $balance - $total;

            $rel1 = $member->find($receiveid);

            if (empty($receiveid)) {

                $this->error("赠与人恰号不能为空！");
            }

            if (empty($rel1)) {

                $this->error("赠与人不是商城会员！");
            }

            if ($rel < 0) {

                $this->error("可兑换金额不足！");
            }

            if ($total <= 0 || $total < 50) {

                $this->error("转让金额不符合规范！");
            }

            $time = time();

            $_POST['score_no'] = get_number(0);

            $_POST['create_time'] = $time;

            $_POST['update_time'] = $time;

            $_POST['deal_status'] = 1;

            $data = $mebscore->create();

            if ($data) {

                $mebscore->add();

                $data = $member->where(array('id' => $userid))->setDec('balance', $total);

                $data1 = $member->where(array('id' => $receiveid))->setInc('recharge', $total);

                if ($data && $data1) {

                    $this->success("积分转让成功！");
                } else {

                    $this->error("积分转让失败！");
                }
            }
        }

        $member = get_user_info_wechat($this->uid);

        $this->assign('id', $member['id']);

        $this->assign('balance', $member['balance']);

        $this->display();
    }

    /*
     * 根据会员id查询会员信息
     */

    public function check() {

        $id = I('post.id');

        $member = M('Member');

        $m_find = $member->find($id);

        //echo M()->getLastSql();
        //echo json_encode($m_find);
        if (empty($m_find)) {

            $this->error('**您输入的会员ID不存在**', '0');
        } else {

            echo $m_find['info'];
        }
    }

}
