<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace WeChat\Controller;

/**
 * 微信消息控制器
 * @author xzake
 */
class NewsController extends BaseController {

    /**
     * 编辑分组
     * @author xzake
     */
    public function detail($id) {

        $info = D('CustomReplyNews')->find($id);
        $this->assign("info", $info);
        $this->assign("meta_title", $info['title']);
        $this->display();
    }

    public function keyword($kw) {

        $map['keyword'] = array("eq", $kw);
        $info = D("CustomReplyNews")->where($map)->find();
        $this->assign("info", $info);
        $this->assign("meta_title", $info['title']);
        $this->display('detail');
    }

}
