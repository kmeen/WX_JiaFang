<?php

namespace WeChat\Controller;

use Think\Log;

class QrcodeController extends BaseController {

    private $uid = 0;

    protected function _initialize() {
        parent::_initialize();

        //$this->uid =  $this->login();
        $this->uid = 6065;
    }

    /*
     * 邀请入口；链接
     */

    public function invite($id) {

        echo $this->login(TRUE, $id);

        redirect(U("WeChat/Index/index"));
    }

    /*
     * 会员二维码信息
     * 
     * @param 微信二维码 和 其他二维码
     */

    public function member_qrcode() {

        $url_qrcode = get_user_info_wechat($this->uid, 'url_qrcode');

        if (empty($url_qrcode)) {

            $member_info = get_user_info_wechat($this->uid);

            //获取微信二维码步骤
            //一、获取基础access_token 此值有效期7200s, 重复获取会导致上次的失效。每天有调用次数限制
            $this->wechat->getToken();  //获取基础access_token
            //二、获取用户永久二维码 根据access_token 获取ticket（传入的场景值会在微信返回 消息中返回给服务器处理）
            //根据 ticket获取到用户微信二维码
            $url_qrcode = $this->wechat->getQRUrl($this->uid, TRUE); //$this->uid 场景值id(scene_id)

            $storage_qrcode_url = M('Member')->where('id=' . $this->uid)->setField('url_qrcode', $url_qrcode);

            $base_url = THINK_PATH . "../Public/front/headimg/"; //文件存储项目路径

            if ($storage_qrcode_url) {

                $http = new \Org\Net\Http();

                $image = new \Think\Image();

                //下载会员的二维码信息
                $qrcode_info = downloadImageFromWeiXin($url_qrcode);

                $member_qrcode_name = $base_url . $member_info['id'] . 'q' . '.jpg';

                $local_file = fopen($member_qrcode_name, 'w');

                if (false !== $local_file) {

                    if (false != fwrite($local_file, $qrcode_info['body'])) {

                        fclose($local_file);
                    }
                }


                //下载会员头像
                $head_url = $member_info['info']['headimgurl'];

                //判段头像不存在的情况
                if (empty($head_url)) {

                    $head_url = THINK_PATH . "../Public/front/images/noavatar_big.jpg";
                }

                $head_name = $member_info['id'] . 'h' . '.jpg';

                $head_load = $base_url . $head_name;

                $http->curlDownload($head_url, $head_load);


                //会员头像 、二维码 与 背景图结合生成一张图片
                $url_background = THINK_PATH . "../Public/front/images/icon/erweima.jpg";

                //处理会员头像生成缩略图
                $url_head = $base_url . $member_info['id'] . 'h' . '.jpg';
                $img_head = $image->open($url_head);
                $img_head->thumb(150, 150, \Think\Image::IMAGE_THUMB_SCALE)->save($url_head);
                //dump($a);exit;
                //处理会员二维码生成缩略图
                $url_qrcode = $base_url . $member_info['id'] . 'q' . '.jpg';
                $img_qrcode = $image->open($url_qrcode);
                $img_qrcode->thumb(380, 380, \Think\Image::IMAGE_THUMB_SCALE)->save($url_qrcode);

                $target_img = Imagecreatefromjpeg($url_background);
                $target_img_size = getimagesize($url_background);
                //dump($target_img_size);
                $head_img = Imagecreatefromjpeg($url_head);
                $qrcode_img = Imagecreatefromjpeg($url_qrcode);

                $center_x = ($target_img_size[0] - 380) / 2;

                //imagecopy php原生图片处理函数 拷贝图像的一部分
                //下例中：即拷贝qrcode_img 图像
                imagecopy($target_img, $qrcode_img, $center_x, 280, 0, 0, 380, 380);
                imagecopy($target_img, $head_img, $center_x, 90, 0, 0, 150, 150);

                //生成新图片名称
                $create_img_url = $base_url . $member_info['id'] . '.jpg';

//用户字符串昵称

                $nickname = '【' . get_str($member_info['nickname'], 0, 12) . '】';

//定义字体名称。有些字体不能显示汉字，多试几次！
                $font = "C:\Windows\Fonts\msyhbd.ttf";

                $font_color = imagecolorallocate($target_img, 255, 255, 255); //字体颜色
                imagefttext($target_img, 20, 0, 340, 130, $font_color, $font, '我是：');
                imagefttext($target_img, 20, 0, 340, 180, $font_color, $font, $nickname);
                imagefttext($target_img, 20, 0, 340, 230, $font_color, $font, '我为XX商城代言！');

                $create_img = Imagejpeg($target_img, $create_img_url, 100);

                if ($create_img) {

                    $qrcode_url = 'public/front/headimg/' . $member_info['id'] . '.jpg';
                }
            }
        }

        $this->assign('qrcode_url', $qrcode_url);

        $this->display();
    }

    /**
     * 我的二维码
     */
    public function qrcode() {

        $http = new \Org\Net\Http();

        $image = new \Think\Image();

        $id = is_login_wechat();

        $member = M('Member')->find($id);
        //$this->assign("member",$member);
        if ($member['share_qrcode_status'] <> 1) {

            $info = json_decode($member['info']);

            $qrcode = $member['url_qrcode'];

            if (empty($member['ticket']) || empty($qrcode)) {

                $uid = $this->login(FALSE);

                $this->wechat->getToken();

                $qrcode = $this->wechat->getQRUrl($id, TRUE);

                $ticket = $this->wechat->getTicket();
                //echo $uid;
                M("Member")->where('id=' . $uid)->setField('ticket', $ticket);

                M('Member')->where('id=' . $uid)->setField('url_qrcode', $qrcode);
            }

//获取用户的微信头像        
            $hurl = $info->headimgurl;
            $hsavename = $member['id'] . 'h' . '.jpg';
            $hload = THINK_PATH . "../Public/front/headimg/" . $hsavename;
            $http->curlDownload($hurl, $hload);

//获取用户的二维码
            $qurl = $qrcode;
            $q_info = downloadImageFromWeiXin($qurl);
            $qsavename = THINK_PATH . "../Public/front/headimg/" . $member['id'] . 'q' . '.jpg';
            $local_file = fopen($qsavename, 'w');
            //dump($local_file);
            if (false !== $local_file) {
                if (false != fwrite($local_file, $q_info['body'])) {
                    fclose($local_file);
                }
            }

//将用户头像和二维码 与背景图片 合成一张图片
            $url_bg = THINK_PATH . "../Public/front/images/icon/erweima.jpg";

            $url_head = THINK_PATH . "../Public/front/headimg/" . $member['id'] . 'h' . '.jpg';
            $img_head = $image->open($url_head);
            $url_head_edit = THINK_PATH . "../Public/front/headimg/" . $member['id'] . 'h' . '.jpg';
            $img_head->thumb(150, 150, \Think\Image::IMAGE_THUMB_SCALE)->save($url_head_edit);
            //dump($a);exit;
            $url_qrcode = THINK_PATH . "../Public/front/headimg/" . $member['id'] . 'q' . '.jpg';
            $img_qrcode = $image->open($url_qrcode);
            $url_qrcode_edit = THINK_PATH . "../Public/front/headimg/" . $member['id'] . 'q' . '.jpg';
            $img_qrcode->thumb(380, 380, \Think\Image::IMAGE_THUMB_SCALE)->save($url_qrcode_edit);

            $target_img = Imagecreatefromjpeg($url_bg);
            $target_img_size = getimagesize($url_bg);
            //dump($target_img_size);
            $head_img = Imagecreatefromjpeg($url_head_edit);
            $qrcode_img = Imagecreatefromjpeg($url_qrcode_edit);

            $center_x = ($target_img_size[0] - 380) / 2;

            imagecopy($target_img, $qrcode_img, $center_x, 280, 0, 0, 380, 380);
            imagecopy($target_img, $head_img, $center_x, 90, 0, 0, 150, 150);


            $url_success = THINK_PATH . '../Public/front/headimg/' . $member['id'] . '.jpg';
//用户字符串昵称
            $str = $member['nickname'];
            $str = '【' . get_str($str, 0, 12) . '】';

//定义字体名称。有些字体不能显示汉字，多试几次！
            $font = "C:\Windows\Fonts\msyhbd.ttf";

            $black = imagecolorallocate($target_img, 255, 255, 255); //字体颜色
            imagefttext($target_img, 20, 0, 340, 130, $black, $font, '我是：');
            imagefttext($target_img, 20, 0, 340, 180, $black, $font, $str);
            imagefttext($target_img, 20, 0, 340, 230, $black, $font, '我为纽扣商城代言！');

            $a = Imagejpeg($target_img, $url_success);
            if ($a) {
                $src = 'public/front/headimg/' . $member['id'] . '.jpg';
                M('Member')->where(array('id' => $id))->setField('share_qrcode_status', '1');
            }
        }

        $src = 'public/front/headimg/' . $member['id'] . '.jpg';

        $this->assign('src', $src);
        $this->display();
    }

}
