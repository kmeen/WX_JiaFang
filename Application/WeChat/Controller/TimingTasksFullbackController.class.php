<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WeChat\Controller;

use Think\Controller;

/**
 * Description of TimingTasksController
 * 
 * 定时任务控制器
 *
 * @author xingji
 */
class TimingTasksController extends Controller {

    //put your code here
    protected $is_open_fullback = 0;  //是否开启全返 0、未开启 1、已开启
    protected $stop_fullback = 0.00;  //累计收入达到多少，停止全返
    protected $recommended_level_fullback = 1; //获得全返的 推荐等级条件 （这里为直推）
    protected $recommended_number_fullback = 2; //获取全返的  等级内人数 
    protected $scale_fullback = 0;     //获取全返比例
    protected $fullback_type = 0;       //全返方式 0手动 1、自动

    function _initialize() {

        //读取数据库中的配置
        $config = S('DB_CONFIG_DATA');

        if (!$config) {

            $config = D('SystemConfig')->lists();

            S('DB_CONFIG_DATA', $config);
        }

        C($config); //添加配置

        $this->is_open_fullback = C('IS_OPEN_FULLBACK');
        $this->stop_fullback = C('OT_STOP_FULLBACK');
        $this->recommended_level_fullback = C('FB_RECOMMENDED_LEVEL');
        $this->recommended_number_fullback = C('FB_RECOMMENDED_NUMBER');
        $this->scale_fullback = C('FB_SCALE');
        $this->fullback_type = C('C_FULLBACK_TYPE');
    }

    /*
     * 全返定时任务
     * 
     * @description 全返金额以平台每日收益的钱进行计算
     */

    public function member_rewards_fullback() {
        
        //判段今天是否已经全返分红
        $fullback_time = D('MemberFullback')->order('id desc')->getField('create_time');
        
        $timestamp_today = get_time(1, 0, 0);
        
        if(($fullback_time > $timestamp_today['min']) and ($fullback_time < $timestamp_today['max'])){
            
            echo '全返分红今天已经处理！'; exit;
        }

        if (!$this->is_open_fullback) {

            echo '全返模块处于禁用状态，请先开启！';
            exit;
        }

        //查找满足全返条件的会员
        //全返等级($this->recommended_level_fullback) 和 全返等级人数($this->recommended_number_fullback)
        if (($this->recommended_level_fullback == 0) || ($this->recommended_number_fullback <= 0)) {

            echo '全返推荐等级条件 和 全返推荐会员数 设置有误，请查看！';
            exit;
        }

        //查询出全返会员信息等级和人数限制
        $array_members_fullback_info = D('Member')->get_members_fullback($this->recommended_level_fullback, $this->recommended_number_fullback, $this->stop_fullback);

        if (!$array_members_fullback_info['status']) {

            echo $array_members_fullback_info['msg'];
        }

        $members_fullback_info = $array_members_fullback_info['msg'];

        if (empty($members_fullback_info)) {

            echo '没有满足全返条件的会员！';
            exit;
        }

        //平台今日收益
        $time = get_time(1, 0, 1); //一天时间内的时间戳数组

        $array_platform_income = D('Order')->get_order_amount($time, 1, 1);
        
        $platform_income = $array_platform_income['msg'];

        if (!$platform_income['status'] || ($platform_income == 0)) {

            echo '近日平台收益为0.00，无法进行全返！';
            exit;
        }


        $scale = $this->scale_fullback;

        //判段分佣比例
        if ($scale <= 0) {

            echo '全返分红比例为0，请到后台设置!';
            exit;
        }

        /*         * *******************************开始计算全返************************************** */

        // 人数 、 钱 和 全返比例（占平台今日收益的比例）
        
        $num = count($members_fullback_info);

        $totay_member_fullback_rewords = $platform_income * $scale / 100 / $num;

        $add_fullback_members = D('MemberFullback')->add_members_fullback($members_fullback_info, $totay_member_fullback_rewords, $this->fullback_type, $platform_income, $this->scale_fullback);

        if (!$add_fullback_members['status']) {

            echo $add_fullback_members['msg'];
        } else {

            echo $add_fullback_members['msg'];
        }
    }

    
    /*
     * test the function
     */

    public function test() {
        
    }

}
