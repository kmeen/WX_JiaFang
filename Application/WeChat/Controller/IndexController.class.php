<?php

namespace WeChat\Controller;

class IndexController extends BaseController {

    protected function _initialize() {

        parent::_initialize();

      $this->login();
    //  $this->uid=400;//====delete
    }

    /*
     * 前台首页展示
     */

    public function index() {

        $category_model = D('Category');

        $goods_model = D('Goods');

        $c_map['status'] = 1; //0、禁用 1、启用

        $c_map['show_index'] = 1; //0、首页不展示 1、首页展示

        $category = $category_model->where($c_map)->order('sort')->select(); //所有启用分类


        $ids = array_map('reset', $category); //函数取出分类二维数组中的第一个键值内容
        #replace method
        /**

          $ids = array();

          foreach ($category as $v) {

          $ids[] = $v['id'];
          }
         */
        $p_map['category'] = array('in', $ids);

        $p_map['status'] = 1;

        $products = $goods_model->where($p_map)->order(' is_hot desc, sort desc, id desc')->select();
//        echo M()->getLastSql();
//        die();
        foreach ($category as $key => $v) {

            foreach ($products as $p) {

                if ($p['category'] == $v['id']) {                            
                    $category[$key]["list"][] = $p;
                }
            }
        }
 
        //删除没有产品的分类
        foreach ($category as $k => $v) {
            if (empty($v['list'])) {
                unset($category[$k]);
            }
        }
        //每个分类显示4个
        foreach ($category as $key => $value) {
        
              $category[$key]["list"]=array_slice($category[$key]["list"],0,4);
        }

        $this->assign("data", $category);

        $this->assign('banner_list', $this->get_banner());  //轮播图
//        $uid = is_login_wechat();
//        if ($uid) {
//            $member = get_user_info_wechat($uid);
//            $member["info"] = json_decode($member['info'], TRUE);
//            $this->assign("member", $member);
//            $this->assign("start_goods_link", U('WeChat/Goods/detail', array('id' => C('FX_STARTED_GOODS'))));
//        }

        $this->display();
    }

    /*
     * 轮播图
     */

    public function get_banner() {

        $banner_list = D('Carousel')->where(' status = 1 ')->order('id')->select();

        return $banner_list;
    }

}
