<?php

namespace WeChat\Controller;

class SpecialController extends BaseController {

    protected function _initialize() {
        parent::_initialize();
        $this->login();
    }

    public function detail($id) {
        $uid = is_login_wechat();

        if ($uid) {
            $member = get_user_info_wechat($uid);
            $member["info"] = json_decode($member['info'], TRUE);
            $this->assign("member", $member);
        }
        //$member = get_user_info_wechat($this->uid);
        switch ($id) {
            case 1:
                $this->assign("start_goods_link", U('WeChat/Goods/detail', array('id' => 35)));
                $this->display("detail_1");
                break;
            case 2:
                $this->assign("start_goods_link", U('WeChat/Goods/detail', array('id' => 36)));
                $this->display("detail_2");
                break;
            case 3:
                $this->assign("start_goods_link", U('WeChat/Goods/detail', array('id' => 34)));
                $this->display("detail_3");
                break;
            case 4:
                $this->assign("start_goods_link", U('WeChat/Goods/detail', array('id' => 33)));
                $this->display("detail_4");
                break;

            default:
                $this->assign("start_goods_link", U('WeChat/Goods/detail', array('id' => 35)));
                $this->display("detail_1");
                break;
        }
    }

}
