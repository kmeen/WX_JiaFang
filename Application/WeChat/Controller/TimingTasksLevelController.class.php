<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WeChat\Controller;

use Think\Controller;
use Think\log;

/**
 * 定时任务-会员等级分红
 * 
 *
 * @author xingji
 */
class TimingTasksLevelController extends Controller {

    //put your code here

    function _initialize() {

        //读取数据库中的配置
        $config = S('DB_CONFIG_DATA');

        if (!$config) {

            $config = D('SystemConfig')->lists();

            S('DB_CONFIG_DATA', $config);
        }

        C($config); //添加配置
        //等级会员团队分红是否开启

        if (!C('IS_OPEN_TEAMREWARS')) {

            echo '团队共创奖励处于禁用状态，请联系管理员开启！';
            exit;
        }
    }

    /*
     * 等级会员的团队共创奖励
     */

    public function teamRewards_level_members() {

        //今日等级会员团队分红处理判段
        $teamdividend_time = D('MemberTeamdividend')->order('id desc')->getField('create_time');

        $timestamp_today = get_time(1, 0, 0);

        if (($teamdividend_time > $timestamp_today['min']) and ( $teamdividend_time < $timestamp_today['max'])) {

            echo '等级会员的团队分红今天已经处理！最后处理时间：' . date('Y-m-d H:i', $teamdividend_time);
            exit;
        }

        //筛选一部分用户
        $members = D('Member')->where(' total_spend > 0 and (rgt - lft - 1)/2 >=1 and is_lock <> 1')->order('id')->select();

//       dump($members);
        //判段出当前所有会员的等级信息 member['level']
        foreach ($members as $k => $v) {

            $array_member_level = D('MemberLevel')->member_level($v['id']);

//           dump($array_member_level);

            if (!$array_member_level['status']) {

                echo '会员-' . $v['id'] . '判段等级信息失败，原因-' . $array_member_level['msg'];
                continue;
            }

            if (!$array_member_level['msg']['status']) {

                unset($members[$k]);
                continue;
            }

            $members[$k]['level_title'] = $array_member_level['msg']['level'];
            $members[$k]['level_scale'] = $array_member_level['msg']['scale'];
        }

        if (empty($members)) {

            echo '没有符合条件的团队分红会员！' . date('Y-m-d', time());
            exit;
        }

        //将等级会员按团队(flag)划分整合

        $members_team = $this->merge_teams($members);

        //昨日时间
        $limit_time = get_time(1, 0, 1);

        //一个团队一个团队处理
        foreach ($members_team as $k => $v) {

            $team_create = 0.00;

            $team_num = count($v);

            if ($team_num == 1) {

                $array_team_create = D('Order')->get_memberteam_create($v[0]['id'], $limit_time);

                if (!$array_team_create['status']) {

                    echo $array_team_create['msg'];
                    continue;
                }

                $team_create = $array_team_create['msg']['num'];

                if ($team_create == 0) {

                    echo '等级-' . $v['level_title'] . '-用户id-' . $v[0]['id'] . '的团队共创为0-' . date('Y-m-d H:i');
                    
                    continue;
                }

                //添加团队分红数据

                $row = D('MemberTeamdividend')->add_memberteam_rewards($v[0], $team_create, 0, null);
                
            } else {

                foreach ($v as $kk => $vv) {

                    //同等级会员信息
                    $samelevel_members = $this->get_team_samelevel_member($members_team[$k], $vv);

                    $samelevel_members_num = count($samelevel_members);


//                    $samelevel_members_info = json_encode($samelevel_members);

                    //团队共创
                    $array_team_create = D('Order')->get_memberteam_create($vv['id'], $limit_time);

                    if (!$array_team_create['status']) {

                        echo $array_team_create['msg'];
                        continue;
                    }

                    $team_create = $array_team_create['msg']['num'];

                    if ($team_create == 0) {

                        echo '等级-' . $vv['level_title'] . '-用户id-' . $vv['id'] . '的团队共创为0-' . date('Y-m-d H:i');
                        continue;
                    }

                    //添加团队分红数据

                    $row = D('MemberTeamdividend')->add_memberteam_rewards($vv, $team_create, $samelevel_members_num, $samelevel_members);
                }
            }

            //判段row
            log::write('团队分红信息：' . $row['msg']);
        }
    }

    /**
     * 将所有等级会员 按 团队合并到一起
     * 
     * @param array members  所有(等级会员)二维数组
     * 
     * @param string item 比较的
     * 
     */
    private function merge_teams($members, $field = 'flag') {

        $members_team = array();

        foreach ($members as $k => $v) {

            $members_team[$v[$field]][] = $v;
        }

        return $members_team;
    }

    /*
     * 获取指定会员团队下与自己同等级的会员数
     * 
     * @param teams_member  同一团队所有等级会员信息
     * 
     * @param member_info 指定会员的团队
     */

    public function get_team_samelevel_member($teams_member, $member_info) {

        $samelevel_member = array();

        foreach ($teams_member as $k => $v) {

            if (($v['lft'] <= $member_info['lft']) || ($v['lvl'] <= $member_info['lvl'])) {
                continue;
            }

            $samelevel_member[] = $v;
        }

        return $samelevel_member;
    }

}
