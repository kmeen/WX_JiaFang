<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

use Common\Model\MemberModel;

use Think\Page;
/**
 * 会员控制器
 * @author xzake
 */
class MemberController extends AdminController {

    public function index() {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['id|nickname'] = array($condition, $condition, '_multi' => true);

        //获取所有分类
        $map['status'] = array('egt', '0'); //禁用和正常状态
        $p = I("p");
        $data_list = D('Member')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('id desc')->select();
        $page = new \Common\Util\Page(D('Member')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('会员列表')  //设置页面标题
                ->addResumeButton() //添加启用按钮
                ->addForbidButton() //添加禁用按钮
                ->setSearch('请输入ID/昵称', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('nickname', '昵称', 'text')
                ->addField('lvl_name', '会员级别', 'text')
                ->addField('name', '真实姓名', 'text')
                ->addField('phone', '手机', 'text')
                ->addField('wechat', '微信号', 'text')                
               ->addField('total_spend', '累计消费', 'text')
                ->addField('distribution_money', '累计佣金', 'text')
                ->addField('manage_money', '累计董事奖', 'text')    
                ->addField('create_time', '关注时间', 'time')
                ->addField('manage', '董事', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表 
                ->addRightButton('link', '', 'Member/mdetail', '详情')
                ->addRightButton('manage')
                ->setPage($page->show())
                ->display();
    }

    /*
     * 佣金详情
     * 
     */

    public function mdetail($tab = 0, $id) {
        $map['user_id'] = $id;
        switch ($tab) {
            case 0:
                $map['pay_status'] = 2;
                $map['status'] = 1;
                $total = M('Order')->where($map)->sum('total');
                $list = M('Order')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('create_time desc')->select();
                $page = new \Common\Util\Page(D('Order')->where($map)->count(), C('ADMIN_PAGE_ROWS'));
                break;
            case 1:
                $map['pay_status'] = 2;
                $map['send_status'] = 2;
                $total = M('Order')->where($map)->sum('total');
                $list = M('Order')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('create_time desc')->select();
                $page = new \Common\Util\Page(D('Order')->where($map)->count(), C('ADMIN_PAGE_ROWS'));
                break;
            case 2:
                $map['status'] = 2;
                $total = M('Order')->where($map)->sum('total');
                $list = M('Order')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('create_time desc')->select();
                $page = new \Common\Util\Page(D('Order')->where($map)->count(), C('ADMIN_PAGE_ROWS'));
                break;
            case 3:
                $total = M('MemberCash')->where(array('status' => 1, 'userid' => $id))->sum('total');
                $list = M('MemberCash')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('create_time desc')->select();
                $page = new \Common\Util\Page(D('MemberCash')->where($map)->count(), C('ADMIN_PAGE_ROWS'));
                break;
            case 4:
                $member = M('Member')->find($id);
                $cmap['sid'] = 1;
                $cmap['flag'] = $member['flag'];
                $cmap['lvl'] = array('between', array($member['lvl'] + 1, $member['lvl'] + 3));
                $total = M('Member')->where($cmap)->count();
                $list = M('Member')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($cmap)->order('create_time desc')->select();
                $page = new \Common\Util\Page(D('MemberCash')->where($map)->count(), C('ADMIN_PAGE_ROWS'));
                break;
        }

        $this->assign("list", $list);
        $this->assign('total', $total);
        $this->assign('page', $page);
        $this->assign("page", $page->show());
        $this->assign("tab_list", array("已付款订单", "已收货订单", "已取消订单", "提现信息", "会员团队"));
        $this->assign("current_tab", $tab);
        $this->assign("uid", $id);
        $this->assign("meta_title", "会员详情");

        $this->display();
    }

    /*
     * 会员统计
     */

    public function statistics() {

        $today = strtotime(date('Y-m-d', time())); //今天
        $start_date = I('get.start_date') ? I('get.start_date') / 1000 : $today - 14 * 86400;
        $end_date = I('get.end_date') ? (I('get.end_date') + 1) / 1000 : $today + 86400;
        $count_day = ($end_date - $start_date) / 86400; //查询最近n天
        $user_object = D('Member');
        for ($i = 0; $i < $count_day; $i++) {
            $day = $start_date + $i * 86400; //第n天日期
            $day_after = $start_date + ($i + 1) * 86400; //第n+1天日期
            $map['create_time'] = array(
                array('egt', $day),
                array('lt', $day_after)
            );
            $user_reg_date[] = date('m月d日', $day);
            $user_reg_count[] = (int) $user_object->where($map)->count();
        }

        $this->assign('burl', U("Member/statistics"));
        $this->assign('start_date', date('Y年m月d日', $start_date));
        $this->assign('end_date', date('Y年m月d日', $end_date - 1));
        $this->assign('count_day', $count_day);
        $this->assign('user_reg_date', json_encode($user_reg_date));
        $this->assign('user_reg_count', json_encode($user_reg_count));
        $this->assign('meta_title', "会员统计");
        $this->assign('statistics_title', "会员");

        $this->assign('CONTROLLER_NAME0', CONTROLLER_NAME);

        $this->display("Public/statistics");
    }

    public function delCallback($model, $map) {
        
    }

    /*
     *  执行董事
     *  Author: cys 2016-11-21
     */

    public function manageList() {

        $map['manage'] = array('eq', '1'); 
        $p = I("p");
        $data_list = D('Member')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('id desc')->select();
        $page = new \Common\Util\Page(D('Member')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('执行董事列表')  //设置页面标题
                ->addField('id', 'ID', 'text')
                ->addField('nickname', '昵称', 'text')
             ->addField('total_spend', '累计消费', 'text')
                ->addField('distribution_money', '累计佣金', 'text')
                ->addField('manage_money', '累计董事奖', 'text')
              
              ->addField('manage', '董事', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表
                ->addRightButton('link', '', 'Member/manageBonus', '上月分红')
                ->addRightButton('link', '', 'Member/monthMoney', '当月业绩')
                ->setPage($page->show())
                ->display();
    }

      public function manageList_bf() {
        I("p") == ' ' ? $p = 1 : $p = I("p");
//
        $memberModel = new MemberModel();
        $total_list = $manager_list = $memberModel->getManagers(C('FX_M_COUNT'));

        $total_count = count($total_list);

        $manager_list = array_slice($manager_list, ($p - 1) * C('ADMIN_PAGE_ROWS'), C('ADMIN_PAGE_ROWS'));
      
        $page = new \Common\Util\Page($total_count, C('ADMIN_PAGE_ROWS'));

        $builder = new \Common\Builder\ListBuilder();
        $builder->title('会员列表')  //设置页面标题
                ->addField('id', 'ID', 'text')
                ->addField('nickname', '昵称', 'text')
                ->addField('status', '状态', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($manager_list)    //数据列表
                ->addRightButton('link', '', 'Member/manageBonus', '分红')
                ->setPage($page->show())
                ->display();
        
    }
    
     /*
    *  单独给执行董事分红
    *  获取下面团队的月业绩，从成为执行董事时开始
     */
    public function manageBonus() {

         $user_id=I ('get.id');
        $user_id=='' ? $user_id= I('post.user_id') : $user_id=I ('get.id'); 
            
         $member_list =  D('MemberManagerewards')->get_manage_team($user_id);

           $last_month = getdate(strtotime("-1 month"));//上个月时间
           //$last_month['mon']=11; ===delete
           $last_month_time = mFristAndLast($last_month['year'], $last_month['mon']);
    
           $month_bonus = 0;
           foreach ($member_list as $key=>$value){
              $conditon['create_time'] = array("between", array($last_month_time[0], $last_month_time[1]));
             $conditon['pay_status'] = 2;
             $conditon['status'] = 1;
             $conditon['user_id'] = $value['id'];
             $user_month_bonus = M("order")->where($conditon)->field("total")->sum("total");
    
            $month_bonus+=$user_month_bonus;
           }

           if (IS_POST) {          
                    $data = array(
                                       'user_id' => I('post.user_id'),
                                        'money'  => I('post.money'),
                                       'total'  => $month_bonus,
                                       'scale'  => C('FX_M_SCARE'),
                                       'create_time' => NOW_TIME,
           	       'month' => $last_month_time[0] 	      
           	         );
                     M('MemberManagerewards')->add($data);        
                    M('Member')->where(array('id' => I('post.user_id')))->setInc('manage_money',  I('post.money')); //累加会员分佣金额
                     $this->success('分红成功', U('Member/manageList/'));
           }

            $last_share  = M('MemberManagerewards')->where('user_id='.$user_id)->order('id desc')->find();         
           if ($last_share['month'] == $last_month_time[0]) {
               $this->assign('shared', 'shared');
               $this->assign('money', $last_share['money']);
           }  
        
            $this->assign('user_id', $user_id);
            $this->assign('last_mon', $last_month['mon']);
            $this->assign('total_money', $month_bonus);
            $this->assign('scale', C('FX_M_SCARE'));
            $this->assign('share_money', $month_bonus * C('FX_M_SCARE') / 100);
            $this->display('share_bonus');            
            
    } 
    
   public function relation_old()
     {
         $data_list = D('Member')->where("pid=0")->field('id,nickname')->select();
         $this->assign('parent', $data_list);
         $this->display();     
     }
     
     public function relation()
     {
         $data_list = D('Member')->where("pid=0")->field('id,nickname')->select();
         $Page = new Page(count($data_list), 30);
         $user_list = D('Member')->where("pid=0")
                                                        ->order("`sort` asc")
                                                        ->limit($Page->firstRow . ',' . $Page->listRows)
                                                        ->select();
         $show = $Page->show();
  
         $this->assign('show', $show);
         $this->assign('parent', $user_list);
         $this->display();
     }
    
    public function ajax_get_child(){
  
       $child_list = D('Member')->where('pid='. I('get.id'))->field('id,nickname')->select();
        echo json_encode($child_list);        
    }     
    
    //获取当月团队业绩
    public function monthMoney() {
    
        $user_id=I ('get.id');
    
        $member_list =  D('MemberManagerewards')->get_manage_team($user_id);

        $this_month = getdate(strtotime("0 month"));//上个月时间

        $this_month_time = mFristAndLast($this_month['year'], $this_month['mon']);
     
         $time['min'] = $this_month_time[0];
         $time['max'] = $this_month_time[1];
        $month_bonus = 0;
        $order_list = array();
        $members=array();
        foreach ($member_list as $k=>$v){
            $members[$k]=$v['id'];
        }

        $list = D('Order')->get_membersteam_order_amount($members, $time, '微信支付', 0);
        $list['status']==false ? $list['msg']['num'] = 0 : true;
        $this->assign('time', $this_month);
        $this->assign('list', $list);
        $this->display();
    
    }
    
    
    
    
}
