<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 后台默认控制器
 * @author xzake
 */
class IndexController extends AdminController {

    /**
     * 默认方法
     * @author xzake
     */
    public function index() {

        $today = strtotime(date('Y-m-d', time())); //今天

        $start_date = I('get.start_date') ? I('get.start_date') / 1000 : $today - 14 * 86400;
        $end_date = I('get.end_date') ? (I('get.end_date') + 1) / 1000 : $today + 86400;
        $count_day = ($end_date - $start_date) / 86400; //查询最近n天
        $user_object = D('Member');
        for ($i = 0; $i < $count_day; $i++) {
            $day = $start_date + $i * 86400; //第n天日期
            $day_after = $start_date + ($i + 1) * 86400; //第n+1天日期
            $map['create_time'] = array(
                array('egt', $day),
                array('lt', $day_after)
            );
            $user_reg_date[] = date('m月d日', $day);
            $user_reg_count[] = (int) $user_object->where($map)->count();
        }


        $all_order_count = M('Order')->count();
        $all_member_count = M('Member')->count();
        $omap['create_time'] = array('egt', $today);
        $today_order_count = M('Order')->where($omap)->count();
        $today_member_count = M('Member')->where($omap)->count();
        $this->assign('all_order_count', $all_order_count);
        $this->assign('all_member_count', $all_member_count);
        $this->assign('today_order_count', $today_order_count);
        $this->assign('today_member_count', $today_member_count);

        $this->assign('start_date', date('Y年m月d日', $start_date));
        $this->assign('end_date', date('Y年m月d日', $end_date - 1));
        $this->assign('count_day', $count_day);
        $this->assign('user_reg_date', json_encode($user_reg_date));
        $this->assign('user_reg_count', json_encode($user_reg_count));
        $this->assign('meta_title', "首页");
        $this->display('Public/index');
    }

    /**
     * 完全删除指定文件目录
     * @author xzake
     */
    public function rmdirr($dirname = RUNTIME_PATH) {
        $file = new \Common\Util\File();
        $result = $file->del_dir($dirname);
        if ($result) {
            $this->success("缓存清理成功");
        } else {
            $this->error("缓存清理失败");
        }
    }

    public function delCallback($model, $map) {
        
    }

}
