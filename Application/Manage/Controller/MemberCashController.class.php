<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 
 * 会员提现控制器
 * 
 * @author
 */
class MemberCashController extends AdminController {

    public function index($tab = 0) {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['user_id|account_name|phone'] = array($condition, $condition, $condition, '_multi' => true);

        //获取所有分类
        if ($tab == 0) {
            $map['status'] = array('eq', '0'); //禁用和正常状态
        } else {
            $map['status'] = array('eq', '1'); //禁用和正常状态
        }
        $p = I("p");
        $data_list = D('MemberCash')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('create_time')->select();
        $page = new \Common\Util\Page(D('MemberCash')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        $this->assign('data_list', $this);
        $this->assign('page', $page);
        $this->assign("tab_list", array("未处理", "已处理"));
        $this->assign("current_tab", $tab);
        $this->assign("data_list", $data_list);
        $this->assign("page", $page->show());
        $this->assign("meta_title", "订单列表");
        $this->assign("search", array('title' => "请输入用户ID/申请人/手机号", 'url' => U('index', array("tab" => $tab))));

        $this->display();
    }

    public function cancel($id) {
        $r = D('MemberCash')->where("id = " . $id)->setField("status", 0);
        if ($r !== false) {
            $this->success('取消成功');
        } else {
            $this->error('取消失败');
        }
    }

    public function grant($id) {
        $r = D('MemberCash')->where("id = " . $id)->setField("status", 1);
        if ($r !== false) {
            $this->success('操作成功');
        } else {
            $this->error('操作失败');
        }
    }

    public function delCallback($model, $map) {
        
    }

}
