<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 微信控制器
 * @author xzake
 */
class WeXinController extends AdminController {

    public function setting($id = 1) {
        //获取商品信息
        $info = D('Wechat')->find($id);

        if (IS_POST) {
            $user_object = D('Wechat');

            if ($info) {
                if ($user_object->save($_POST) !== false) {
                    $this->success('更新成功', U('setting'));
                } else {
                    $this->error('更新失败', $user_object->getError());
                }
            } else {
                $_POST['id'] = 1;
                $id = $user_object->add($_POST);
                if ($id) {
                    $this->success('新增成功', U('setting'));
                } else {
                    $this->error('新增失败');
                }
            }
        } else {
            $info = D('Wechat')->find($id);
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('设置微信公众号')  //设置页面标题
                    ->setUrl(U('setting')) //设置表单提交地址
                    ->addItem('id', 'hidden', 'ID', 'ID')
                    ->addItem('wechat_name', 'text', '公众号名称', '公众号名称')
                    ->addItem('wechat_id', 'text', '原始ID', '原始ID')
                    ->addItem('wechat', 'text', '微信号', '微信号')
                    ->addItem('token', 'text', 'token', 'token')
                    ->addItem('appid', 'text', 'appid', 'appid')
                    ->addItem('secret', 'text', 'secret', 'secret')
                    ->addItem('encodingaeskey', 'text', 'Encodingaeskey', 'Encodingaeskey')
                    ->addItem('mch_id', 'text', '商户号', '微信支付商户号')
                    ->addItem('paykey', 'text', '商户密钥', '微信支付商户密钥')
                    ->setFormData($info)
                    ->display();
        }
    }

    public function menus() {
        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('微信')  //设置页面标题
                ->display();
    }

    public function reply() {
        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('微信')  //设置页面标题
                ->display();
    }

    public function multi() {
        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('微信')  //设置页面标题
                ->display();
    }

    public function delCallback($model, $map) {
        
    }

}
