<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 微信控制器
 * @author xzake
 */
class CustomReplyNewsController extends AdminController {

    /**
     * 分组列表
     * @author xzake
     */
    public function index() {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['id|title|keyword'] = array($condition, $condition, $condition, '_multi' => true); //搜索条件
        //获取所有分组
        $map['status'] = array('egt', '0'); //禁用和正常状态
        $p = I("p");
        $data_list = D('CustomReplyNews')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('sort desc, id desc')->select();
        $page = new \Common\Util\Page(D('CustomReplyNews')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('分组列表')  //设置页面标题
                ->AddNewButton()    //添加新增按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/分组名称', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('keyword', '关键字', 'text')
                ->addField('title', '标题', 'text')
                ->addField('cover', '图片', 'image')
                ->addField('create_time', '创建时间', 'time')
                ->addField('update_time', '更新时间', 'time')
                ->addField('sort', '排序', 'text')
                ->addField('status', '状态', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表
                ->addRightButton('edit')   //添加编辑按钮
                ->addRightButton('delete') //添加删除按钮
                ->setPage($page->show())
                ->display();
    }

    /**
     * 新增分组
     * @author xzake
     */
    public function add() {
        if (IS_POST) {
            $keyword = I("post.keyword");
            $map['keyword'] = array('eq', $keyword);
            $iss = D('Keyword')->where($map)->select();
            if (!empty($iss)) {
                $this->error('关键字已经存在!');
            }
            $user_group_object = D('CustomReplyNews');
            $data = $user_group_object->create();
            if ($data) {
                $id = $user_group_object->add($data);
                if ($id) {
                    $kdata["keyword"] = $keyword;
                    $kdata["type"] = "CustomReplyNews";
                    $kdata["type_key"] = $id;
                    $kdata["keyword_length"] = strlen($keyword);
                    D('Keyword')->add($kdata);
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('新增图文')  //设置页面标题
                    ->setUrl(U('add')) //设置表单提交地址
                    ->addItem('keyword', 'text', '关键字', '关键字')
                    ->addItem('title', 'text', '标题', '标题')
                    ->addItem('intro', 'textarea', '介绍', '介绍')
                    ->addItem('cover', 'picture', '标题', '标题')
                    ->addItem('url', 'text', '外链', '填写外链时详细内容不要填写')
                    ->addItem('content', 'kindeditor', '详细内容', '详细内容')
                    ->addItem('sort', 'text', '排序', '排序')
                    ->display();
        }
    }

    /**
     * 编辑分组
     * @author xzake
     */
    public function edit($id) {
        if (IS_POST) {

            $keyword = I("post.keyword");
            $map['keyword'] = array('eq', $keyword);
            $map['type_key'] = array('neq', $id);
            $iss = D('Keyword')->where($map)->select();
            if (!empty($iss)) {
                $this->error('关键字已经存在!');
            }

            $user_group_object = D('CustomReplyNews');
            $data = $user_group_object->create();
            if ($data) {
                if ($user_group_object->save() !== false) {
                    $kdata["keyword"] = $keyword;
                    $kdata["keyword_length"] = strlen($keyword);

                    $kmap["type"] = "CustomReplyNews";
                    $kmap["type_key"] = $id;
                
                    D('Keyword')->where($kmap)->save($kdata);

                    $this->success('更新成功', U('index'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            $info = D('CustomReplyNews')->find($id);

            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('编辑用户')  //设置页面标题
                    ->setUrl(U('edit')) //设置表单提交地址
                    ->addItem('id', 'hidden', 'ID', 'ID')
                    ->addItem('keyword', 'text', '关键字', '关键字')
                    ->addItem('title', 'text', '标题', '标题')
                    ->addItem('intro', 'textarea', '介绍', '介绍')
                    ->addItem('cover', 'picture', '标题', '标题')
                    ->addItem('url', 'text', '外链', '填写外链时详细内容不要填写')
                    ->addItem('content', 'kindeditor', '详细内容', '详细内容')
                    ->addItem('sort', 'text', '排序', '排序')
                    ->setFormData($info)
                    ->display();
        }
    }

    public function delCallback($model, $map) {

        $kmap['type_key'] = $map['id'];
        $kmap['type'] = $model;

        D("Keyword")->where($kmap)->delete();
    }

}
