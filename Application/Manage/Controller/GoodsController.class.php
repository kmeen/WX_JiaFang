<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 商品-控制器
 * @author xzake
 */
class GoodsController extends AdminController {

    public function index() {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['id|title'] = array($condition, $condition, '_multi' => true);

        //获取所有产品
        $p = I("p");
        $data_list = D('Goods')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('is_recommend,sort desc,id desc')->select();
        $page = new \Common\Util\Page(D('Goods')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        foreach ($data_list as $k => $v) {
            $category_id = $v['category'];
            $cate_title = D('Category')->field('title')->find($category_id);
            $data_list[$k]['category'] = $cate_title['title'];

            $data_list[$k]['is_recommend'] = $v['is_recommend'] ? '是' : '否';
        }

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('产品列表')  //设置页面标题
                ->AddNewButton()    //添加新增按钮
                ->addResumeButton("", "上架") //添加启用按钮
                ->addForbidButton("", "下架") //添加禁用按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/产品名', U('index'))
                ->addField('id', '产品ID', 'text')
                ->addField('sort', '排序', 'text')
                ->addField('category', '所属分类', 'text')
                ->addField('cover', '图片', 'image')
           
                ->addField('title', '商品标题', 'text')
                ->addField('price', '价格', 'text')
               ->addField('inventory', '库存', 'text')
//                ->addField('pv', 'PV值', 'text')
         //    ->addField('inventory', '库存', 'text')
           //     ->addField('click_count', '点击量', 'text')
               // ->addField('is_recommend', '是否推荐', 'text')
                ->addField('status', '状态', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表
                ->addRightButton('edit')   //添加编辑按钮
                ->addRightButton('putaway') //添加禁用/启用按钮
                ->addRightButton('delete') //添加删除按钮
                ->setPage($page->show())
                ->display();
    }

    /**
     * 新增商品
     * @author xzake
     */
    public function add() {
        if (IS_POST) {

            //商品详情图处理
            $covers = I('param.cover_multi');
            $_POST['cover_multi'] = implode(',', $covers);

            $goods_object = D('Goods');
            $data = $goods_object->create();
            if ($data) {
                $id = $goods_object->add();
                if ($id) {
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($goods_object->getError());
            }
        } else {
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('新增商品')  //设置页面标题
                    ->setUrl(U('add')) //设置表单提交地址
                    ->addItem('cover', 'picture', '商品封面', '')
                    ->addItem('cover_multi', 'picture-multi', '详情图片')
                    ->addItem('category', 'select', '选择分类', '', $this->selectListAsTree('Category'))
//                    ->addItem('special_category', 'radio', '所属类别', '', C('Category_icon'))
                    ->addItem('title', 'text', '商品名称', '')
                    ->addItem('price', 'text', '价格', '价格')
                    ->addItem('original_price', 'text', '原价', '原价')
//                    ->addItem('commission', 'text', '佣金', '佣金')
//                    ->addItem('pv', 'text', 'PV值')
                    ->addItem('is_recommend', 'radio', '是否推荐', '', array('否', '是'))
                    ->addItem('inventory', 'text', '库存', '库存')
                    ->addItem('detail', 'kindeditor', '产品详情', '产品详情')
                    ->addItem('status', 'radio', '是否上架', '', array('下架', '上架'))
                    ->addItem('sort', 'text', '排序', '根据数值倒序排列')
                    ->display();
        }
    }

    /**
     * 编辑商品
     * @author xzake
     */
    public function edit($id) {

        if (IS_POST) {
            //商品详情图处理
            $covers = I('param.cover_multi');
            $_POST['cover_multi'] = implode(',', $covers);

            $goods_object = D('Goods');

            $data = $goods_object->create();

            if ($goods_object->save() !== false) {

                $this->success('更新成功', U('index'));
            } else {

                $this->error('更新失败', $goods_object->getError());
            }
        } else {
            $info = D('Goods')->find($id);
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('编辑商品')  //设置页面标题
                    ->setUrl(U('edit')) //设置表单提交地址
                    ->addItem('id', 'hidden', 'ID', 'ID')
                    ->addItem('cover', 'picture', '图片', '商品封面')
                    ->addItem('cover_multi', 'picture-multi', '详情图片')
                    ->addItem('category', 'select', '分类', '所属分类', $this->selectListAsTree('Category'))
//                    ->addItem('special_category', 'radio', '所属类别', '', C('Category_icon'))
                    ->addItem('title', 'text', '名称', '商品名称')
                    ->addItem('price', 'text', '价格', '价格')
                    ->addItem('original_price', 'text', '原价', '原价')
//                    ->addItem('commission', 'text', '佣金', '佣金')
//                    ->addItem('pv', 'text', 'PV值', 'PV值')
                    ->addItem('is_recommend', 'radio', '是否推荐', '', array('否', '是'))
                    ->addItem('inventory', 'text', '库存', '库存')
                    ->addItem('detail', 'kindeditor', '产品详情', '产品详情')
                    ->addItem('status', 'radio', '是否上架', '', array('下架', '上架'))
                    ->addItem('sort', 'text', '排序', '产品排序')
                    ->setFormData($info)
                    ->display();
        }
    }

    public function delCallback($model, $map) {
        
    }

}
