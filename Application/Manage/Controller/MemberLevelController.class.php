<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 会员等级控制器
 * @author xzake
 */
class MemberLevelController extends AdminController {

    public function index() {
        //搜索
        $keyword = (string) I('keyword');

        $condition = array('like', '%' . $keyword . '%');

        $map['id|title'] = array($condition, $condition, '_multi' => true);

        $map['status'] = array('egt', '0'); //禁用和正常状态

        $data_list = D('MemberLevel')->where($map)->order('id')->select();

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();

        $builder->title('会员等级列表')  //设置页面标题
                ->AddNewButton()    //添加新增按钮
//                ->addResumeButton() //添加启用按钮
//                ->addForbidButton() //添加禁用按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/昵称', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('title', '昵称', 'text')
                ->addField('recommended_number_min', '直推最低会员数', 'text')
                ->addField('recommended_number_max', '直推最高会员数', 'text')
                ->addField('scale', '分红比例', 'text')
                ->addField('update_time', '编辑时间', 'time')
                ->addField('status', '状态', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表
                ->addRightButton('edit')   //添加编辑按钮
//                ->addRightButton('forbid') //添加禁用/启用按钮
                ->addRightButton('delete') //添加删除按钮
                ->display();
    }

    /**
     * 新增会员等级
     * @author xzake
     */
    public function add() {

        if (IS_POST) {

            $level_object = D('MemberLevel');

            $data = $level_object->create();

            if ($data) {

                $id = $level_object->add();

                if ($id) {

                    $this->success('新增成功', U('index'));
                } else {

                    $this->error('新增失败');
                }
            } else {

                $this->error($level_object->getError());
            }
        } else {
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('新增会员等级')  //设置页面标题
                    ->setUrl(U('add')) //设置表单提交地址
                    ->addItem('title', 'text', '昵称', '')
                    ->addItem('recommended_number_min', 'text', '直推最低人数', '')
                    ->addItem('recommended_number_max', 'text', '直推最高人数', '')
                    ->addItem('scale', 'text', '分红比例', '团队分红比例[同等级平分]')
                    ->display();
        }
    }

    /*
     * 编辑会员等级
     */

    public function edit($id) {

        if (IS_POST) {

            $level_object = D('MemberLevel');

            $level_object->create();

            if ($level_object->save() !== false) {

                $this->success('编辑成功', U('index'));
            } else {

                $this->error('编辑失败');
            }
        } else {

            $info = D('MemberLevel')->find($id);
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('编辑会员等级')  //设置页面标题
                    ->setUrl(U('edit')) //设置表单提交地址
                    ->addItem('id', 'hidden', 'ID', 'ID')
                    ->addItem('title', 'text', '昵称', '')
                    ->addItem('recommended_number_min', 'text', '直推最低人数', '')
                    ->addItem('recommended_number_max', 'text', '直推最高人数', '')
                    ->addItem('scale', 'text', '分红比例', '团队分红比例[同等级平分]')
                    ->setFormData($info)
                    ->display();
        }
    }

    public function delCallback($model, $map) {
        
    }

}
