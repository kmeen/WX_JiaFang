<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 订单控制器
 * @author xzake
 */
class OrderController extends AdminController {

    public function index($tab = 0) {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['order_no|phone|name'] = array($condition, $condition, $condition, '_multi' => true);

        switch ($tab) {

            case 1:

                $map['status'] = 1;

                $map['pay_status'] = 2;

                $map['send_status'] = 1;

                break;
            case 2:

                $map['status'] = 1;

                $map['pay_status'] = 2;

                break;
            case 3:

                $map['status'] = 1;

                $map['pay_status'] = array('lt', 2);

                break;
            case 4:

                $map['status'] = 4;

                break;
            case 5:

                $map['status'] = array('egt', 0);

                break;
            default :

                $map['status'] = 1;
                $map['pay_status'] = 2;
                $map['send_status'] = 0;
        }

        $p = I("p");

        $data_list = D('Order')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))
                        ->where($map)->order('id desc')->select();

        $page = new \Common\Util\Page(D('Order')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        $this->assign('data_list', $this);

        $this->assign('page', $page);

        $this->assign("tab_list", array("待发货订单", "待收货订单", "已付款订单", "待付款订单", "退货申请", "全部订单"));

        $this->assign("current_tab", $tab);

        $this->assign("data_list", $data_list);

        $this->assign("page", $page->show());

        $this->assign("meta_title", "订单列表");

        $this->assign("search", array('title' => "订单号/手机号/收货人", 'url' => U('index', array("tab" => $tab))));

        $this->display();
    }

    /**
     * 
     * 订单详情
     * 
     * @author
     * 
     */
    public function detail($id) {

        $order = D('Order')->find($id);

        if (empty($order)) {

            $this->error('找不到订单信息');
        }

        $goods = D('OrderGoods')->get_ordergoods($id); //ordergoods model get goods

        $mcmap['order_no'] = array('eq', $order['order_no']);

        $commissions_list = D('MemberCommission')->where($mcmap)->select();

//        dump($commissions_list);exit;

        $this->assign('commissions', $commissions_list);

        $this->assign('goods', $goods['msg']);

        $this->assign('order', $order);

        $this->assign('action', ACTION_NAME);

        $this->meta_title = '订单详情';

        $this->display();
    }

    /**
     * 发货
     * @author xzake
     */
    public function sendOut($id) {

        $order = D('Order')->find($id);

        if (IS_POST) {

            $data = I('post.');

            $data['send_status'] = 1; //发货修改订单状态 send_status 1、表示发货

            if (D('Order')->save($data) !== false) {

                $this->success('发货成功', U("index"));
            } else {

                $this->error('操作异常，请重试');
            }
        } else {

            if (empty($order)) {

                $this->error('查询异常，订单信息不存在！' . $id);
            }

            $goods = D('OrderGoods')->get_ordergoods($id);

            $mcmap['order_no'] = array('eq', $order['order_no']);

            $commissions = D('MemberCommission')->where($mcmap)->select();

            $this->assign('commissions', $commissions);

            $this->assign('goods', $goods['msg']);

            $this->assign('order', $order);

            $this->assign('action', ACTION_NAME);

            $this->meta_title = '发货';

            $this->display("detail");
        }
    }

    /**
     * 功能停用
     * 
     * 确认收货后 放佣
     * 
     * @author
     */
    public function giveOut($id) {

        $order = D('Order')->find($id);

        if (empty($order)) {

            $this->error('找不到订单信息999' . $id);
        }

        $mcmap['order_no'] = array('eq', $order['order_no']);

        $commissions = D('MemberCommission')->where($mcmap)->select();

        foreach ($commissions as $item) {

            if ($item['status'] != 1) {

                D('Member')->where('id=' . $item['userid'])->setInc('balance', $item['commission']);
            }
        }

        D('MemberCommission')->where($mcmap)->setField('status', 1);

        $mcmap['id'] = array('eq', $id);

        D('Order')->where($mcmap)->setField('commission_status', 1);

        $this->success('佣金发放成功');
    }

    /**
     * 取消订单
     * @author xzake
     */
    public function cancel($id) {

        $order = D('Order')->find($id);
        
        if (empty($order)) {
            
            $this->error('找不到订单信息');
        }
        
        $map['id'] = array('eq', $id);
        
        if (D('Order')->where($map)->setField("status", 2)) {
            
            $mcmap['order_no'] = array('eq', $order['order_no']);
            
            D('MemberCommission')->where($mcmap)->setField("status", 1);
            
            $this->success('成功取消订单');
        } else {
            $this->error('取消订单失败');
        }
    }

    /**
     * 发货
     * @author xzake
     */
    public function refund($id) {
        if (IS_POST) {

            $keyword = I("post.keyword");
            $map['keyword'] = array('eq', $keyword);
            $iss = D('Order')->where($map)->select();
            if (!empty($iss)) {
                $this->error('关键字已经存在!');
            }

            $user_group_object = D('CustomReplyText');
            $data = $user_group_object->create();
            if ($data) {
                $id = $user_group_object->add();
                if ($id) {
                    $kdata["keyword"] = $keyword;
                    $kdata["type"] = CONTROLLER_NAME;
                    $kdata["type_key"] = $id;
                    $kdata["keyword_length"] = strlen($keyword);
                    D('Keyword')->add($kdata);
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('发货')  //设置页面标题
                    ->setUrl(U('sendOut')) //设置表单提交地址
                    ->addItem('keyword', 'text', '关键字', '关键字')
                    ->addItem('content', 'textarea', '回复内容', '回复内容')
                    ->display();
        }
    }

    public function statistics() {

        $today = strtotime(date('Y-m-d', time())); //今天
        $start_date = I('get.start_date') ? I('get.start_date') / 1000 : $today - 14 * 86400;
        $end_date = I('get.end_date') ? (I('get.end_date') + 1) / 1000 : $today + 86400;
        $count_day = ($end_date - $start_date) / 86400; //查询最近n天
        $order_object = D('Order');
        for ($i = 0; $i < $count_day; $i++) {
            $day = $start_date + $i * 86400; //第n天日期
            $day_after = $start_date + ($i + 1) * 86400; //第n+1天日期
            $map['create_time'] = array(
                array('egt', $day),
                array('lt', $day_after)
            );
            $order_reg_date[] = date('m月d日', $day);
            $order_reg_count[] = (int) $order_object->where($map)->count();
        }
        //计算今日订单数量 及 订单金额
        $tmap['create_time'] = array('between', array($today, $today + 86400));
        $tmap['pay_status'] = '2';
        $tmap['status'] = 1;
        $o_today_num = $order_object->where($tmap)->count('id');
        $o_today_spend = $order_object->where($tmap)->sum('total');

        $moth_time = mFristAndLast();
        $tmap['create_time'] = array('between', array($moth_time[0], $moth_time[1]));
        $o_moth_num = $order_object->where($tmap)->count('id');
        $o_moth_spend = $order_object->where($tmap)->sum('total');

        $this->assign('o_today_num', $o_today_num);
        $this->assign('o_today_spend', $o_today_spend);
        $this->assign('o_moth_num', $o_moth_num);
        $this->assign('o_moth_spend', $o_moth_spend);

        $this->assign('burl', U("Order/statistics"));
        $this->assign('start_date', date('Y年m月d日', $start_date));
        $this->assign('end_date', date('Y年m月d日', $end_date - 1));
        $this->assign('count_day', $count_day);
        $this->assign('user_reg_date', json_encode($order_reg_date));
        $this->assign('user_reg_count', json_encode($order_reg_count));
        $this->assign('meta_title', "订单统计");
        $this->assign('CONTROLLER_NAME0', CONTROLLER_NAME);

        $this->assign('statistics_title', "订单");

        $this->display("Public/statistics");
    }

    public function delCallback($model, $map) {
        
    }

}
