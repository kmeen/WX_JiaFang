<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 分组控制器
 * @author xzake
 */
class UserGroupController extends AdminController {

    /**
     * 分组列表
     * @author xzake
     */
    public function index() {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['id|title'] = array($condition, $condition, '_multi' => true); //搜索条件
        //获取所有分组
        $map['status'] = array('egt', '0'); //禁用和正常状态
        $data_list = D('UserGroup')->where($map)->order('sort asc, id asc')->select();

        //转换成树状列表
        $tree = new \Common\Util\Tree();
        $tree_list = $tree->toFormatTree($data_list);

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('分组列表')  //设置页面标题
                ->AddNewButton()    //添加新增按钮
                ->addResumeButton() //添加启用按钮
                ->addForbidButton() //添加禁用按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/分组名称', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('title_show', '标题', 'text')
                ->addField('sort', '排序', 'text')
                ->addField('status', '状态', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($tree_list)    //数据列表
                ->addRightButton('edit')   //添加编辑按钮
                ->addRightButton('forbid') //添加禁用/启用按钮
                ->addRightButton('delete') //添加删除按钮
                ->display();
    }

    /**
     * 新增分组
     * @author xzake
     */
    public function add() {
        if (IS_POST) {
            $user_group_object = D('UserGroup');
            $_POST['auth'] = implode(',', I('post.auth'));
            $data = $user_group_object->create();
            if ($data) {
                $id = $user_group_object->add();
                if ($id) {
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            $map['status'] = array('egt', 0);
            $tree = new \Common\Util\Tree();
            $all_group = $tree->toFormatTree(D('UserGroup')->where($map)->order('sort asc,id asc')->select());
            $all_group = array_merge(array(0 => array('id' => 0, 'title_show' => '顶级分组')), $all_group);
            $this->assign('all_group', $all_group);
            $this->meta_title = '新增分组';
            $this->display('Public/group');
        }
    }

    /**
     * 编辑分组
     * @author xzake
     */
    public function edit($id) {
        if (IS_POST) {
            $user_group_object = D('UserGroup');
            $_POST['auth'] = implode(',', I('post.auth'));
            $data = $user_group_object->create();
            if ($data) {
                if ($user_group_object->save() !== false) {
                    $this->success('更新成功', U('index'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            $info = D('UserGroup')->find($id);
            $info['auth'] = explode(',', $info['auth']);
            $map['status'] = array('egt', 0);
            $tree = new \Common\Util\Tree();
            $all_group = $tree->toFormatTree(D('UserGroup')->where($map)->order('sort asc,id asc')->select());
            $all_group = array_merge(array(0 => array('id' => 0, 'title_show' => '顶级分组')), $all_group);
            $this->assign('all_group', $all_group);
            $this->assign('info', $info);
            $this->meta_title = '编辑分组';
            $this->display('Public/group');
        }
    }

    public function delCallback($model, $map) {
        
    }

}
