<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://weipig.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://weipig.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 轮播控制器
 * @author xzake
 */
class CarouselController extends AdminController {

    public function index() {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['id|title'] = array($condition, $condition, '_multi' => true);

        //获取所有产品
        $map['status'] = array('egt', '0'); //禁用和正常状态
        $p = I("p");
        $data_list = D('Carousel')->page(!empty($p) ? $p : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('sort desc,id desc')->select();
        $page = new \Common\Util\Page(D('Carousel')->where($map)->count(), C('ADMIN_PAGE_ROWS'));


        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('产品列表')  //设置页面标题
                ->AddNewButton()   //添加新增按钮
                ->addForbidButton() //添加禁用按钮
                ->addResumeButton() //添加启用按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/名称', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('title', '名称', 'text')
                ->addField('cover', '封面', 'image')
                ->addField('create_time', '上传时间', 'time')
                ->addField('status', '状态', 'status')
                ->addField('sort', '排序', 'text')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表
                ->addRightButton('edit')   //添加编辑按钮
                ->addRightButton('forbid') //添加禁用/启用按钮
                ->addRightButton('delete') //添加删除按钮
                ->setPage($page->show())
                ->display();
    }

    /**
     * 新增轮播图
     * @author xzake
     */
    public function add() {
        if (IS_POST) {
            $user_object = D('Carousel');
            $data = $user_object->create();
            if ($data) {
                $id = $user_object->add();
                if ($id) {
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($user_object->getError());
            }
        } else {
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('新增轮播图')  //设置页面标题
                    ->setUrl(U('add')) //设置表单提交地址
                    ->addItem('cover', 'picture', '封面', '轮播图封面')
                    ->addItem('title', 'text', '标题', '轮播图名称')
                    ->addItem('link', 'text', '外链', '选填')
                    ->addItem('sort', 'text', '排序', '首页轮播顺序')
                    ->display();
        }
    }

    /**
     * 编辑轮播图
     * @author xzake
     */
    public function edit($id) {
        //获取轮播图信息
        $info = D('Carousel')->find($id);

        if (IS_POST) {
            $user_object = D('Carousel');
            $data = $user_object->create();

            if ($user_object->save() !== false) {
                //echo M()->getLastSql();
                $this->success('更新成功', U('index'));
            } else {
                $this->error('更新失败', $user_object->getError());
            }
        } else {
            $info = D('Carousel')->find($id);
            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->title('编辑轮播图')  //设置页面标题
                    ->setUrl(U('edit')) //设置表单提交地址
                    ->addItem('id', 'hidden', 'ID', 'ID')
                    ->addItem('cover', 'picture', '封面', '轮播图封面')
                    ->addItem('title', 'text', '标题', '轮播图名称')
                    ->addItem('link', 'text', '外链', '选填')
                    ->addItem('sort', 'text', '排序', '首页轮播顺序')
                    ->setFormData($info)
                    ->display();
        }
    }

    public function delCallback($model, $map) {
        
    }

}
