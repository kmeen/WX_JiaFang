<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 商品分类控制器
 * @author xzake
 */
class CategoryController extends AdminController {

    public function index() {
        
        //搜索
        $keyword = (string) I('keyword');
        
        $condition = array('like', '%' . $keyword . '%');
        
        $map['id|title'] = array($condition, $condition, '_multi' => true);

        //获取所有分类
        $data_list = D('Category')->where($map)->order('sort, id')->select();

        //转换成树状列表
        $tree = new \Common\Util\Tree();
        $tree_list = $tree->toFormatTree($data_list);
        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('分类列表')  //设置页面标题
                ->AddNewButton()    //添加新增按钮
                ->addResumeButton() //添加启用按钮
                ->addForbidButton() //添加禁用按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/分类名', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('title_show', '分类名称', 'text')
                ->addField('cover', '分类图片', 'image')
                ->addField('show_index', '主页显示', 'status')
                ->addField('status', '状态', 'status')
                ->addField('sort', '排序', 'text')
                ->addField('right_button', '操作', 'btn')
                ->dataList($tree_list)    //数据列表
                ->addRightButton('edit')   //添加编辑按钮
                ->addRightButton('forbid') //添加禁用/启用按钮
                ->addRightButton('delete') //添加删除按钮
                ->display();
    }

    /**
     * 新增分组
     * @author xzake
     */
    public function add() {
        if (IS_POST) {
            $user_group_object = D('Category');
            $data = $user_group_object->create();
            if ($data) {
                $id = $user_group_object->add();
                if ($id) {
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            $map['status'] = array('egt', 0);
            $tree = new \Common\Util\Tree();
            $all_group = $tree->toFormatTree(D('Category')->where($map)->order('sort asc,id asc')->select());
            $all_group = array_merge(array(0 => array('id' => 0, 'title_show' => '顶级分组')), $all_group);
            $this->assign('all_group', $all_group);
            $this->assign('show_index_option', C("SHOW_SELECT_OPTION"));
            $this->meta_title = '新增分组';
            $this->display('Public/category');
        }
    }

    /**
     * 编辑分组
     * @author xzake
     */
    public function edit($id) {
        if (IS_POST) {
            $user_group_object = D('Category');
            $data = $user_group_object->create();
            if ($data) {
                if ($user_group_object->save() !== false) {
                    $this->success('更新成功', U('index'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($user_group_object->getError());
            }
        } else {
            $info = D('Category')->find($id);
            $map['status'] = array('egt', 0);
            $tree = new \Common\Util\Tree();
            $all_group = $tree->toFormatTree(D('Category')->where($map)->order('sort asc,id asc')->select());
            $all_group = array_merge(array(0 => array('id' => 0, 'title_show' => '顶级分组')), $all_group);
            $this->assign('all_group', $all_group);
            $this->assign('show_index_option', C("SHOW_SELECT_OPTION"));
            //dump(C("SHOW_SELECT_OPTION"));
            $this->assign('info', $info);
            $this->meta_title = '编辑分组';
            $this->display('Public/category');
        }
    }

    public function delCallback($model, $map) {
        
    }

}
