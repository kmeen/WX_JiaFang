<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Manage\Controller;

/**
 * 后台上传控制器
 * @author xzake
 */
class UploadController extends AdminController {

    /**
     * 上传列表
     * @author xzake
     */
    public function index() {
        //搜索
        $keyword = (string) I('keyword');
        $condition = array('like', '%' . $keyword . '%');
        $map['id|path'] = array($condition, $condition, '_multi' => true);

        //获取所有上传
        $map['status'] = array('egt', '0'); //禁用和正常状态
        $data_list = D('Upload')->page(!empty($_GET["p"]) ? $_GET["p"] : 1, C('ADMIN_PAGE_ROWS'))->where($map)->order('sort desc,id desc')->select();
        $page = new \Common\Util\Page(D('Upload')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->title('上传列表')  //设置页面标题
                ->addResumeButton() //添加启用按钮
                ->addForbidButton() //添加禁用按钮
                ->addDeleteButton() //添加删除按钮
                ->setSearch('请输入ID/上传关键字', U('index'))
                ->addField('id', 'ID', 'text')
                ->addField('path', '路径', 'text')
                ->addField('size', '大小', 'text')
                ->addField('ctime', '创建时间', 'time')
                ->addField('sort', '排序', 'text')
                ->addField('status', '状态', 'status')
                ->addField('right_button', '操作', 'btn')
                ->dataList($data_list)    //数据列表
                ->addRightButton('forbid') //添加禁用/启用按钮
                ->addRightButton('delete') //添加删除按钮
                ->setPage($page->show())
                ->display();
    }

    /**
     * 上传
     * @author xzake
     */
    public function upload() {
        exit(D('Upload')->upload());
    }

    /**
     * KindEditor编辑器下载远程图片
     * @author xzake
     */
    public function downremoteimg() {
        exit(D('Upload')->downremoteimg());
    }

    /**
     * KindEditor编辑器文件管理
     * @author xzake
     */
    public function fileManager() {
        exit(D('Upload')->fileManager());
    }

    public function delCallback($model, $map) {
        
    }

}
