<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="cn">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>
            <?php if(isset($meta_title)): echo ($meta_title); ?> - <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>
                <?php else: ?> 
                <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN'); endif; ?>
        </title>

        <meta name="description" content=" <?php echo C('WEB_SITE_DESCRIPTION');?>" />

        <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>

        <script src="/Public/jquery/jquery-1.11.1.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <!-- basic styles -->
        <!--首页轮播和商品多图js-->
        <link rel="stylesheet" href="/Public/front/css/flexslider.css" />

        <link rel="stylesheet" href="/Public/front/css/comman.css" />

        <link rel="stylesheet" href="/Public/front/css/style.css" />

        <link rel="stylesheet" href="/Public/front/css/milk.css" />

        <link rel="stylesheet" href="/Public/front/css/user2015.css" />

        <link rel="stylesheet" href="/Public/front/css/tpl.css" />

        <link rel="stylesheet" href="/Public/front/css/style12.css" />

        <script src="/Public/front/js/jquery.min.js"></script> 

        <script src="/Public/front/js/jquery_dialog.js"></script>

        <!--会话框js和css-->
        <link rel="stylesheet" href="/Public/remind/css/alert.css" />

        <script src="/Public/remind/js/jquery.alerts.js"></script>
        <!--会话框js和css end-->

        <script src="/Public/front/js/jquery.flexslider-min.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <script src="/Public/front/js/time.js"></script>

        <script src="/Public/front/js/user.js"></script>

        <!--调用微信js接口 2016-7-22 17:57:35-->
        <!--<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>-->

        <script type="text/javascript">
            //通过config接口注入权限验证配置
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。

                appId: '<?php echo ($jsappid); ?>', // 必填，公众号的唯一标识

                timestamp: '<?php echo ($jstimestamp); ?>', // 必填，生成签名的时间戳

                nonceStr: '<?php echo ($jsnonceStr); ?>', // 必填，生成签名的随机串

                signature: '<?php echo ($jsSign); ?>', // 必填，签名

                jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage"] // 必填，需要使用的JS接口列表
            });
            //通过ready接口处理成功验证
            wx.ready(function () {

                wx.onMenuShareTimeline({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Index/index.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击分享到朋友圈');
                    },
                    success: function (res) {
                        //alert(res.title);
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }

                });

                wx.onMenuShareAppMessage({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Index/index.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    desc: "<?php echo C('WEB_SITE_DESCRIPTION');?>", // 分享描述

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击发送给朋友');
                    },
                    success: function (res) {
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }
                });

            });

            wx.error(function (res) {
                // alert(res);
                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

            });
        </script>

    </head>

    <body>



<!--顶部关注提示-->
<?php if(!empty($member)): ?><style>

        .hCenter {
            margin: 0px auto;
            font-family: sans-serif;
            text-shadow: none;
        }
        .w {
            width: 100%;
            max-width: 640px;
        }

        .focusC{
            position:fixed;height:50px;line-height:50px;
            background:#653300;
            top: 0;
            z-index: 105;
            position:reative;
        }
        .focusCReplace{
            height:50px;line-height:20px;
        }
        .focusC .img{
            float:left;max-height:35px;max-width:35px;
            margin-left:10px;
            margin-top:8px;
        }
        .focusC .desc{
            float:left;
            color:#FFF;
            margin-left:8px;
            margin-top:7px;
            line-height:20px;
            font-size:11px;
        }
        .focusC .nick{
            color:#00ef00;
        }
        .focusC .action{
            position:absolute;
            color:#FFF;
            border:1px solid #3bea2d;
            border-radius: 2px;
            /*height:20px;*/
            padding:3px 3px;
            line-height:20px;
            top:12px;
            right:10px;

        }
    </style>

    <!--    <div class="w hCenter focusC"> 
    
            <img class="img"  style="width: 35px;height: 35px" src="<?php echo ((isset($member["info"]["headimgurl"]) && ($member["info"]["headimgurl"] !== ""))?($member["info"]["headimgurl"]):'/Public/front/images/noavatar_big.jpg'); ?>" /> 
    
            <span class="desc"> 
    
                <span class="nick">
                    [<?php echo ((isset($member["nickname"]) && ($member["nickname"] !== ""))?($member["nickname"]):'NoBody'); ?>]
                </span>
    
                <br> 您还没有分销资格，赶快获得吧！
            </span> 
    
            <a class="action" href="<?php echo ($start_goods_link); ?>">立即购买</a> 
    
            <div class="c"></div> 
    
        </div>--><?php endif; ?>

<!--end 顶部关注提示结束-->


<!--首页轮播图--start-->
<div class="flexslider">


    <ul class="slides" >

        <?php if(empty($banner_list)): ?><li>
                <img alt="首页轮播" style="width:100%" src="/Public/front/images/lvgtactivity.jpg">
            </li>

            <li>
                <a href="<?php echo U('WeChat/Special/detail/id/4.html');?>"><img alt="首页轮播" style="width:100%" src="/Public/front/images/bulangshan_shucha.jpg"></a>
            </li>

            <li>
                <a href="<?php echo U('WeChat/Special/detail/id/3.html');?>"><img alt="首页轮播" style="width:100%" src="/Public/front/images/bulangshan_shengcha.jpg"></a>
            </li>

            <li>
                <a href="<?php echo U('WeChat/Special/detail/id/1.html');?>"><img alt="首页轮播" style="width:100%" src="/Public/front/images/yecha_sanfen.jpg"></a>
            </li>

            <li>
                <a href="<?php echo U('WeChat/Special/detail/id/2.html');?>"><img alt="首页轮播" style="width:100%" src="/Public/front/images/yecha_wufen.jpg"></a>
            </li>

            <?php else: ?>

            <?php if(is_array($banner_list)): $i = 0; $__LIST__ = $banner_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$bl): $mod = ($i % 2 );++$i;?><!--<a href="<?php echo ((isset($bl["link"]) && ($bl["link"] !== ""))?($bl["link"]): 'javascript:;'); ?>">-->
                <li>
                    <img alt="首页轮播" style="width:100%" src="<?php echo (get_cover($bl["cover"])); ?>" />
                </li>
                <!--</a>--><?php endforeach; endif; else: echo "" ;endif; endif; ?>

    </ul>
</div>

<script>
    // JavaScript Document
    $(document).ready(function () {
        $('.flexslider').flexslider({
//            namespace: 'flex-', //控件的命名空间，会影响样式前缀 
            animation: "slide", //String: Select your animation type, "fade" or "slide"图片变换方式：淡入淡出或者滑动
            slideDirection: "horizontal", //String: Select the sliding direction, "horizontal" or "vertical"图片设置为滑动式时的滑动方向：左右或者上下
            controlsContainer: ".home", // 控制容器	.flexslider上一级

            slideshowSpeed: 5000, // 自动播放速度毫秒
            animationSpeed: 600, //滚动效果播放时长
            pausePlay: false, //是否显示播放暂停按钮
        });
    });
</script>

<!--首页轮播图结束-->

<!--正文内容-->
<div id="main">

    <!--结合底部分类效果-->
    <div style="display: none;" id="opquyu"></div>

<div id="opquyubox" style="display:none">
    
    <div style="line-height:26px;">
        
        <?php if(!empty($category)): if(is_array($category)): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$c): $mod = ($i % 2 );++$i;?><h2 style="border-bottom:1px solid #ededed;"><a href="<?php echo U('WeChat/Category/index',array('id'=>$c['id']));?>"><?php echo ($c["title"]); ?></a></h2><?php endforeach; endif; else: echo "暂时没有数据" ;endif; endif; ?>
        
    </div>
    
    <div style=" height:45px;"></div>
    
</div>

<!--<script type="text/javascript">
    
    $(document).ready(function ($) {
        
        $('#opquyu').click(function () {
            
            $(this).hide();
            
            $("#opquyubox").hide();
            
        });

    });
    
</script>-->

    <!--首页模块菜单开始-->
    <div class="logoqu">

        <img style="max-height:90px; max-width:90px;" class="logos" src="<?php echo get_logo(C('WEB_SITE_LOGO'));?>">

        <div style="width:70%; float:right" class="menunav">

            <a href="<?php echo U('WeChat/Member/order');?>" style="width:25%; float:right">
                <i style="background:url(/Public/front/images/11.png) no-repeat center;background-size:auto 30px;"></i>
                我的订单
            </a>

            <a onclick="ajaxopquyu()" style="width:25%; float:right">
                <i style="background:url(/Public/front/images/12.png) no-repeat center;background-size:auto 30px;"></i>
                产品分类
            </a>

            <a href="<?php echo U('WeChat/News/keyword',array('kw'=>'如何关注'));?>" style="width:25%; float:right">
                <i style="background:url(/Public/front/images/13.png) no-repeat center;background-size:auto 30px;"></i>
                如何关注
            </a>

            <a href="<?php echo U('WeChat/News/keyword',array('kw'=>'公司简介'));?>" style="width:25%; float:right">
                <i style="background:url(/Public/front/images/15.png) no-repeat center;background-size:auto 30px;"></i>
                公司简介
            </a>

        </div>

    </div>

    <!--首页菜单模块结束-->

    <!--滚动广告开始-->
    <link rel="stylesheet" href="/Public/front/css/scropAd.css" />

    <div id="gongao"> 
        <div class="remind-img">
            <!--<img src="/Public/front/images/members_only/todayHandlines.png" width="50px" />-->
            <p style="padding-top: 4px ;line-height: 36px;color:#ffffff; text-align: center;font-weight: 600">今日头条</p>
        </div>

        <marquee style="width:90%;margin-left: 10%">这是一个神奇的微信商城！</marquee>

    </div>
    <!--滚动广告结束-->

    <!--搜索模块开始-->
    <form id="form1" name="form1" method="post" action='<?php echo U("Goods/search");?>'>

        <div class="index-search">

            <div class="index-input">

                <input name="keyword" value="" placeholder='请输入搜索关键字' />

                <button type="button" onclick="checKeyword()">查 找</button>

            </div>
        </div>

    </form>

    <script>

        function checKeyword() {
            var keyword = $('input[name = keyword]').val();

            if ((keyword == '') || (keyword == '请输入搜索关键字')) {
                //                jAlert('您好，请输入搜索内容。', '');
                return;
            }

            $('#form1').submit();

        }
    </script>

    <style>
        input,button{padding: 0;margin: 0;}
        .index-search{width: 100%; margin: 0 auto; }
        .index-input{ margin:0 auto;border: 1px solid #c8c8c8; position: relative; margin:5px 0; background: #ffffff none repeat scroll 0 0; border-radius: 5px;  background: rgb(255, 255, 255) url(/Public/front/images/search/search.png) no-repeat 3% center; background-size: 20px 20px;}
        .index-input input{width:90%;line-height: 34px; border: none; margin-left: 10%;  outline:none}
        .index-input button{position: absolute;right: -1px;height: 36px;background: #F70808;color: #ffffff; border: none; top: -1px; border-radius: 3px; border-radius: 0 5px 5px 0; padding: 4px;}
    </style>
    <!-- 搜索模块结束-->

    <!--经典模块展示开始-->
    <link rel="stylesheet" href="/Public/front/css/classicmodule.css" />

    <script>

        function sepcial_category(catId) {

            var categoryId = catId;

            var url = "<?php echo U('Category/special_index');?>" + "&sp_id=" + categoryId;

            window.location.href = url;
        }

    </script>

    <!--  <div class="cloumn-box">
        <ul>

            <li>
                <a href="javascript:;" onclick="sepcial_category('1')">

                    <img src="/Public/front/images/members_only/01.png">

                    <p>运动</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('2')">

                    <img src="/Public/front/images/members_only/02.png">

                    <p>电器</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('3')">

                    <img src="/Public/front/images/members_only/03.png">

                    <p>配饰</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('4')">

                    <img src="/Public/front/images/members_only/04.png">

                    <p>孕婴</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('5')">

                    <img src="/Public/front/images/members_only/05.png">

                    <p>箱包</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('6')">

                    <img src="/Public/front/images/members_only/06.png">

                    <p>服饰</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('7')">

                    <img src="/Public/front/images/members_only/07.png">

                    <p>数码</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('8')">

                    <img src="/Public/front/images/members_only/08.png">

                    <p>家纺</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="sepcial_category('9')">

                    <img src="/Public/front/images/members_only/09.png">

                    <p>美护</p>

                </a>

            </li>

            <li>

                <a href="javascript:;" onclick="ajaxopquyu()">

                    <img src="/Public/front/images/members_only/10.png">

                    <p>更多</p>

                </a>

            </li>

        </ul>

    </div>-->
    
    <!--经典模块展示结束-->

    <!--中间广告位开始-->
    <img src='/Public/front/images/demo/ad.png' style='width:100%; height: 100px' />
    <!--中间广告位结束-->

    <!--分类商品开始-->
    <div style="">

        <?php if(!empty($data)): if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$c): $mod = ($i % 2 );++$i;?><div style="padding-top:0px" class="indexitem">

                    <p class="ptitle">

                        <span>
                            <a><?php echo ($c["title"]); ?></a>
                        </span>

                        <a href="<?php echo U('WeChat/Category/index',array('id'=>$c['id']));?>" style="float:right; margin-right:5px; color:#32a000">更多&gt;&gt;</a>
                    </p>

                    <!--分类图片展示-->
                    <p>
                        <a href="<?php echo U('Category/index',array('id'=>$c['id']));?>">

                            <?php if($c["cover"] != null ): ?><img src="<?php echo (get_cover($c["cover"])); ?>" alt="<?php echo ($c["title"]); ?>" style="width:100%" />

                                <?php else: ?>

                                <img src="/Public/front/images/demo/banner1.jpg" style="width:100%" /><?php endif; ?>
                        </a>
                    </p>


                    <ul class="goodslists">

                        <?php if(!empty($data)): if(is_array($c["list"])): $i = 0; $__LIST__ = $c["list"];if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$p): $mod = ($i % 2 );++$i;?><li style="width:50%; float:left; position:relative;font-size:14px;border-bottom: 1px solid #f3f3f3">

                                    <div style="padding:4px 4px 8px 4px">

                                        <a style="background:#fff; padding:5px; display:block;" href="<?php echo U('WeChat/Goods/detail',array('id'=>$p['id']));?>">

                                            <div style="height:150px;overflow:hidden; text-align:center; position: relative">
                                                <img alt="<?php echo ($p["title"]); ?>" style="height: auto;max-width:100%; min-height:100px;display:inline;" src="<?php echo (get_cover($p["cover"])); ?>">

                                                <div style='background: rgba(102,102,102,0.6); height:24px;position: absolute;bottom: 0;left: 0;width: 100%'>
                                                    <p style="line-height:24px; overflow:hidden; text-align:center;color:#FFF"><?php echo ($p["title"]); ?></p>
                                                </div>
                                            </div>

                                            <p style="line-height:24px; height:24px; overflow:hidden; color:#32a000">
                                                <span style="float:left">惊喜价:</span>
                                                <b style="font-size:16px; float:left; padding-left:3px;" class="price">￥<?php echo ($p["price"]); ?></b>
                                            </p>


                                            <!--<p style="line-height:20px; height:20px; overflow:hidden; color:#999999">积&emsp;分:<?php echo ($p["pv"]); ?></p>-->


                                            <p style="line-height:20px; height:20px; overflow:hidden; color:#999999">
                                                <del>市场价:￥<?php echo ($p["original_price"]); ?></del>
                                            </p>

                                        </a>

                                    </div>

                                    <a href="<?php echo U('WeChat/Goods/detail',array('id'=>$p['id']));?>">
                                        <span class="buyfals" style=" margin-right:0px;width:55px; height:24px; display:block; text-align:center; line-height:24px; font-size:12px; background:#32a000;border-radius:5px; position:absolute; bottom:13px; right:10px; z-index:10; color:#FFFFFF;">立即购买</span>
                                    </a>

                                </li><?php endforeach; endif; else: echo "暂时没有数据" ;endif; endif; ?>

                        <div class="clear"></div>

                    </ul>

                </div><?php endforeach; endif; else: echo "暂时没有数据" ;endif; endif; ?>

    </div>
    <!--分类商品结束-->

</div>

<!--底部导航开始-->
<div style="-webkit-transform:translate3d(0,0,0)" class="top_bar">
    <nav>
        <ul class="top_menu" id="top_menu">
            <li class="li1">
                <a  href="<?php echo U('Index/index');?>">
                    <label>首页</label>
                </a>
            </li>
            <li class="li2">
                <a  onclick="ajaxopquyu()">
                    <label>分类</label>

                </a>
            </li>
            <li class="li4">
                <a  href="<?php echo U('WeChat/Member/center');?>">
                    <label>会员</label>
                </a>
            </li>
            <li class="li5">
                <a style="height:56px; padding:0px"  href="<?php echo U('WeChat/Cart/index');?>">
                    <span style="width:30px; height:32px; display:block; margin:0px auto">
                        <b>
                            <em style="display:block" value="1" class="mycarts" id="buy_price"><?php echo ($cart_count); ?></em>
                        </b>
                    </span>
                    <label>购物车</label>
                </a>
            </li>    
        </ul>
    </nav>
</div>

<style type="text/css">
    body { padding-bottom:60px !important; }
    .top_menu li b {width: 38px;height: 20px;line-height: 17px;display: block;color: #fff;text-align: center;font-size: 12px;}
    .top_menu li b em {padding:0px 3px 0px 3px;border-radius: 100%;text-align: center;background-color: #32a000;display: block;position: absolute;z-index: 9999;margin-top: -10px;margin-left: 22px;}
    user agent stylesheeti, cite, em, var, address, dfn {font-style: italic;}

    .top_menu li.li2 a.butt-cart{display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#32a000;}
    .top_menu li.li4 a.butt-buy {display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#ff6400;}
</style>
</body>
</html>



<!--底部导航结束-->