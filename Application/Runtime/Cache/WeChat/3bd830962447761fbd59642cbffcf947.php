<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="cn">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>
            <?php if(isset($meta_title)): echo ($meta_title); ?> - <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>
                <?php else: ?> 
                <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN'); endif; ?>
        </title>

        <meta name="description" content=" <?php echo C('WEB_SITE_DESCRIPTION');?>" />

        <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>

        <script src="/Public/jquery/jquery-1.11.1.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <!-- basic styles -->
        <!--首页轮播和商品多图js-->
        <link rel="stylesheet" href="/Public/front/css/flexslider.css" />

        <link rel="stylesheet" href="/Public/front/css/comman.css" />

        <link rel="stylesheet" href="/Public/front/css/style.css" />

        <link rel="stylesheet" href="/Public/front/css/milk.css" />

        <link rel="stylesheet" href="/Public/front/css/user2015.css" />

        <link rel="stylesheet" href="/Public/front/css/tpl.css" />

        <link rel="stylesheet" href="/Public/front/css/style12.css" />

        <script src="/Public/front/js/jquery.min.js"></script> 

        <script src="/Public/front/js/jquery_dialog.js"></script>

        <!--会话框js和css-->
        <link rel="stylesheet" href="/Public/remind/css/alert.css" />

        <script src="/Public/remind/js/jquery.alerts.js"></script>
        <!--会话框js和css end-->

        <script src="/Public/front/js/jquery.flexslider-min.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <script src="/Public/front/js/time.js"></script>

        <script src="/Public/front/js/user.js"></script>

        <!--调用微信js接口 2016-7-22 17:57:35-->
        <!--<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>-->

        <script type="text/javascript">
            //通过config接口注入权限验证配置
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。

                appId: '<?php echo ($jsappid); ?>', // 必填，公众号的唯一标识

                timestamp: '<?php echo ($jstimestamp); ?>', // 必填，生成签名的时间戳

                nonceStr: '<?php echo ($jsnonceStr); ?>', // 必填，生成签名的随机串

                signature: '<?php echo ($jsSign); ?>', // 必填，签名

                jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage"] // 必填，需要使用的JS接口列表
            });
            //通过ready接口处理成功验证
            wx.ready(function () {

                wx.onMenuShareTimeline({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Member/members_wallet.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击分享到朋友圈');
                    },
                    success: function (res) {
                        //alert(res.title);
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }

                });

                wx.onMenuShareAppMessage({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Member/members_wallet.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    desc: "<?php echo C('WEB_SITE_DESCRIPTION');?>", // 分享描述

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击发送给朋友');
                    },
                    success: function (res) {
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }
                });

            });

            wx.error(function (res) {
                // alert(res);
                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

            });
        </script>

    </head>

    <body>


<!--<link rel="stylesheet" href="/Public/front/css/brokerage.css" />-->

<style>
    /*佣金*/
    .shouyi{
        border: 1px solid #eaeaea;
        text-align: center;
        background: #fff;
    }
    
    .shouyi img{
        width:23px;
    }

    .shouyi .yue {
        background: #effbdb none repeat scroll 0 0;
        padding: 20px 0 15px;
    }

    .shouyi ul{
        overflow: hidden;
    }
    .shouyi ul li{
        width:50%;
        padding: 20px 0;
        float:left;

    }
    .shouyi h1{
        font-size: 16px;
        font-weight: normal;
        color: #333333;
    }
    .shouyi li p{
        padding: 20px 0 0;
        color: #FF0000;
    }
    .money{
        margin: 30px 0px 80px 0px;
        border: 1px solid #eaeaea;
        background: #fff;

    }
    .money ul{
        overflow: hidden;
    }
    .money li{
        margin-left: -1px;
        text-align: center;
        float: left;
        width: 50%;
        padding: 20px 0;
    }

    .m-img{
        width: 60%;
        margin: 0 auto;
    }
    .m-img img{
        width: 40%;
    }
    .money li h1{
        color: #333333;
        font-size: 18px;
        line-height: 36px;
    }
    .money li p{
        color: #666;
        padding: 10px 0;
    }
    .money span{
        font-size: 16px;
        color: #ff0000;
    }
    
    .money p img{
        width:23px;
    }

    .tixian {
        margin-top: 20px;
    }

    .tixian a {
        color: #666666;
        font-size: 14px;
        padding: 0 15px;
    }
    
</style>

<script>

//    如果为0怎不让跳转
    function  show_list(type, amount){

        if(parseInt(amount) <= -1){
            
            return;
        }

        var url = "<?php echo U('Member/reward_list');?>"+'&status='+type;
    
        window.location.href = url;
    
    }

</script>

<div id="home">

    <div id="header">

        <div style="height:28px; padding-top:10px; background:url(/Public/Front/images/xy.png) 10px 8px no-repeat" class="logo"><span onclick=" history.go( - 1);">&nbsp;</span></div>

        <div class="shoptitle"><span>我的钱包</span></div>

        <div class="logoright" style="z-index: 1000">

    <div>

        <a onclick="ajax_show_menu()" href="javascript:;"></a>

        <div class="showmenu">

            <p>
                <a href="<?php echo U('WeChat/Index/index');?>">返回首页</a>
            </p>

            <p>
                <a href="<?php echo U('WeChat/Member/center');?>">会员中心</a>
            </p>

            <p>
                <a href="<?php echo U('WeChat/Member/order');?>">我的订单</a>
            </p>

            <p>
                <a href="<?php echo U('WeChat/Cart/index');?>">购&nbsp;物&nbsp;车</a>
            </p>

            <p style="border:none">
                <a href="javascript:;" onclick="window.location.reload();">刷新页面</a>
            </p>

        </div>

    </div>

</div>

<script type="text/javascript">
    function ajax_show_menu() {
        $(".showmenu").toggle(500);
    }
</script>

    </div>

</div>

<div style="display: none;" id="opquyu"></div>

<div id="opquyubox" style="display:none">
    
    <div style="line-height:26px;">
        
        <?php if(!empty($category)): if(is_array($category)): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$c): $mod = ($i % 2 );++$i;?><h2 style="border-bottom:1px solid #ededed;"><a href="<?php echo U('WeChat/Category/index',array('id'=>$c['id']));?>"><?php echo ($c["title"]); ?></a></h2><?php endforeach; endif; else: echo "暂时没有数据" ;endif; endif; ?>
        
    </div>
    
    <div style=" height:45px;"></div>
    
</div>

<!--<script type="text/javascript">
    
    $(document).ready(function ($) {
        
        $('#opquyu').click(function () {
            
            $(this).hide();
            
            $("#opquyubox").hide();
            
        });

    });
    
</script>-->

<div id="main">

    <header class="shouyi">

        <div class="yue">

            <h1 style="font-size:18px; font-weight: 600">
                <span style="padding-right: 5px;">

                    <img align="texttop" src="/Public/front/images/brokerage_img/4.png">

                </span>

                余额

            </h1>

            <p style="color: #FF0000;font-size: 18px; padding: 8px; border: 1px solid #eaeaea;width:120px; margin: 15px auto; background: #EAEAEA; border-radius: 8px;">

                <?php echo ($member_balance); ?>

            </p>

            <div class="tixian">

                <a href="<?php echo U('Member/member_cash');?>">立即提现</a> | <a href="<?php echo U('Member/send_score');?>" >点击转赠</a>

            </div>

        </div>

        <ul>

            <li style="">

                <a href="javascript:;">

                    <h1>

                        <span>

                            <img align="absmiddle" src="/Public/front/images/brokerage_img/1.png">

                        </span>

                        已提现金额

                    </h1>

                    <p>&nbsp;&nbsp;&nbsp;￥<?php echo ($member_cash); ?></p>

                </a>

            </li>

            <li style="margin-left: -2px; border-left: 1px solid #EAEAEA;">

                <a href="javascript:;">

                    <h1>

                        <span>

                            <img align="absmiddle" src="/Public/front/images/brokerage_img/2.png">

                        </span>

                        转赠金额

                    </h1>

                    <p>

                        <span style="padding: 0 20px; border-right: 1px solid #EAEAEA;">

                            <img style="width: 22px;" align="absbottom" src="/Public/front/images/brokerage_img/chu.png">

                            &nbsp;<?php echo ($member_score['send']); ?>

                        </span>

                        <span style="padding: 0 20px;">

                            <img align="absbottom" style="width: 22px;" src="/Public/front/images/brokerage_img/ru.png">

                            &nbsp;<?php echo ($member_score['get']); ?>

                        </span>

                    </p>

                </a>

            </li>

        </ul>

    </header>

    <nav class="money">

        <ul>

            <p style="padding:20px 0;border-bottom: 1px solid #EAEAEA;font-size: 16px; text-align: center;">

                <span>

                    <img align="absmiddle" style="margin-right: 10px;" src="/Public/front/images/brokerage_img/3.png">

                </span>

                <span style='font-size:18px; font-weight: 600; color:#000'>总收益：</span><span style='color:#ff0000; font-size:18px'><?php echo ($member_income); ?></span>

            </p>

            <li style="border-right: 1px solid #eaeaea;border-bottom: 1px solid #eaeaea;">

                <a href="javascript:;" onclick='show_list("m_commission", <?php echo ($member_commission); ?>)'>

                    <div class="m-img">

                        <img src="/Public/front/images/brokerage_img/fenyong.png" />

                    </div>

                    <h1>三级分销</h1>

                    <p>总金额：<span>￥<?php echo ($member_commission); ?></span></p>

                </a>

            </li>

            <li style="border-bottom: 1px solid #eaeaea;">

                <a href="javascript:;" onclick='show_list("m_fullback", <?php echo ($member_fullback); ?>)'>

                    <div class="m-img">

                        <img src="/Public/front/images/brokerage_img/quanfan.png">

                    </div>

                    <h1>董事奖励</h1>

                    <p>总金额：<span>￥<?php echo ($manage_teamdividend); ?></span></p>

                </a>

            </li>

         <!--   <li style="border-right: 1px solid #eaeaea">

                <a href="javascript:;" onclick='show_list("m_teamdividend", <?php echo ($member_teamdividend); ?>)'>

                    <div class="m-img">

                        <img src="/Public/front/images/brokerage_img/team.png">

                    </div>

                    <h1>团队奖励</h1>

                    <p>总金额：<span>￥<?php echo ($member_teamdividend); ?></span></p>

                </a>

            </li>

              <li>

                <a href="javascript:;" onclick='show_list("m_publicrow", <?php echo ($member_publicrow); ?>)'>

                    <div class="m-img">

                        <img src="/Public/front/images/brokerage_img/gongpai.png" />

                    </div>

                    <h1>公排奖励</h1>

                    <p>总金额：<span>￥<?php echo ($member_publicrow); ?></span></p>

                </a>

            </li>-->

        </ul>

    </nav>

</div>

<div style="-webkit-transform:translate3d(0,0,0)" class="top_bar">
    <nav>
        <ul class="top_menu" id="top_menu">
            <li class="li1">
                <a  href="<?php echo U('Index/index');?>">
                    <label>首页</label>
                </a>
            </li>
            <li class="li2">
                <a  onclick="ajaxopquyu()">
                    <label>分类</label>

                </a>
            </li>
            <li class="li4">
                <a  href="<?php echo U('WeChat/Member/center');?>">
                    <label>会员</label>
                </a>
            </li>
            <li class="li5">
                <a style="height:56px; padding:0px"  href="<?php echo U('WeChat/Cart/index');?>">
                    <span style="width:30px; height:32px; display:block; margin:0px auto">
                        <b>
                            <em style="display:block" value="1" class="mycarts" id="buy_price"><?php echo ($cart_count); ?></em>
                        </b>
                    </span>
                    <label>购物车</label>
                </a>
            </li>    
        </ul>
    </nav>
</div>

<style type="text/css">
    body { padding-bottom:60px !important; }
    .top_menu li b {width: 38px;height: 20px;line-height: 17px;display: block;color: #fff;text-align: center;font-size: 12px;}
    .top_menu li b em {padding:0px 3px 0px 3px;border-radius: 100%;text-align: center;background-color: #32a000;display: block;position: absolute;z-index: 9999;margin-top: -10px;margin-left: 22px;}
    user agent stylesheeti, cite, em, var, address, dfn {font-style: italic;}

    .top_menu li.li2 a.butt-cart{display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#32a000;}
    .top_menu li.li4 a.butt-buy {display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#ff6400;}
</style>
</body>
</html>