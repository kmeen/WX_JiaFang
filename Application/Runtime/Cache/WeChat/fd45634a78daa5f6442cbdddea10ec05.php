<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="cn">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>
            <?php if(isset($meta_title)): echo ($meta_title); ?> - <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>
                <?php else: ?> 
                <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN'); endif; ?>
        </title>

        <meta name="description" content=" <?php echo C('WEB_SITE_DESCRIPTION');?>" />

        <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>

        <script src="/Public/jquery/jquery-1.11.1.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <!-- basic styles -->
        <!--首页轮播和商品多图js-->
        <link rel="stylesheet" href="/Public/front/css/flexslider.css" />

        <link rel="stylesheet" href="/Public/front/css/comman.css" />

        <link rel="stylesheet" href="/Public/front/css/style.css" />

        <link rel="stylesheet" href="/Public/front/css/milk.css" />

        <link rel="stylesheet" href="/Public/front/css/user2015.css" />

        <link rel="stylesheet" href="/Public/front/css/tpl.css" />

        <link rel="stylesheet" href="/Public/front/css/style12.css" />

        <script src="/Public/front/js/jquery.min.js"></script> 

        <script src="/Public/front/js/jquery_dialog.js"></script>

        <!--会话框js和css-->
        <link rel="stylesheet" href="/Public/remind/css/alert.css" />

        <script src="/Public/remind/js/jquery.alerts.js"></script>
        <!--会话框js和css end-->

        <script src="/Public/front/js/jquery.flexslider-min.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <script src="/Public/front/js/time.js"></script>

        <script src="/Public/front/js/user.js"></script>

        <!--调用微信js接口 2016-7-22 17:57:35-->
        <!--<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>-->

        <script type="text/javascript">
            //通过config接口注入权限验证配置
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。

                appId: '<?php echo ($jsappid); ?>', // 必填，公众号的唯一标识

                timestamp: '<?php echo ($jstimestamp); ?>', // 必填，生成签名的时间戳

                nonceStr: '<?php echo ($jsnonceStr); ?>', // 必填，生成签名的随机串

                signature: '<?php echo ($jsSign); ?>', // 必填，签名

                jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage"] // 必填，需要使用的JS接口列表
            });
            //通过ready接口处理成功验证
            wx.ready(function () {

                wx.onMenuShareTimeline({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Goods/detail/id/1.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击分享到朋友圈');
                    },
                    success: function (res) {
                        //alert(res.title);
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }

                });

                wx.onMenuShareAppMessage({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Goods/detail/id/1.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    desc: "<?php echo C('WEB_SITE_DESCRIPTION');?>", // 分享描述

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击发送给朋友');
                    },
                    success: function (res) {
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }
                });

            });

            wx.error(function (res) {
                // alert(res);
                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

            });
        </script>

    </head>

    <body>


<style type="text/css">

    body{ background:#FFF !important;}

    #main .goods_desc table,#main .goods_desc table td,#main .goods_desc img,#main .goods_desc div img,#main .goods_desc p img,#main .goods_desc table td img{ max-width:100%;}

    .pages a{ padding:1px 5px 1px 5px; border-bottom:2px solid #ccc; border-right:2px solid #ccc; border-left:1px solid #ededed; border-top:1px solid #ededed; margin-left:3px; background:#fafafa}

    .goods_desc img{
        width: 100%;
    }
</style>

<!--商品详情轮播图开始--> 
<div class="flexslider" style="margin-bottom:0px;">
    <ul class="slides">
        <?php if(empty($goods['cover_multi'])): ?><li>
                <img class="ggimg" src='<?php echo (get_cover($goods["cover"])); ?>' width="100%" alt="<?php echo ($goods["title"]); ?>"/>
            </li>
            <?php else: ?>
            <?php if(is_array($goods["cover_multi"])): $i = 0; $__LIST__ = $goods["cover_multi"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$cm): $mod = ($i % 2 );++$i;?><li>
                    <img class="ggimg" src='<?php echo (get_cover($cm)); ?>' width="100%" alt="<?php echo ($goods["title"]); ?>"/>
                </li><?php endforeach; endif; else: echo "" ;endif; endif; ?>
    </ul>
</div>
<script>
    // JavaScript Document
    $(document).ready(function () {
        $('.flexslider').flexslider({
            animation: "fade", //String: Select your animation type, "fade" or "slide"图片变换方式：淡入淡出或者滑动
            controlsContainer: ".home", // 控制容器	.flexslider上一级
            slideDirection: "horizontal", //String: Select the sliding direction, "horizontal" or "vertical"图片设置为滑动式时的滑动方向：左右或者上下
            slideshowSpeed: 4000, // 自动播放速度毫秒
            animationSpeed: 500 //滚动效果播放时长
        });
    });
</script>

<!--商品轮播图结束-->

<!--商品信息开始-->
<div id="main">

    <div class="mainhead" style="padding:5px; border-top:1px solid #ededed;border-bottom:1px solid #ededed;background:#FFF">

        <form id="ECS_FORMBUY" name="ECS_FORMBUY" method="post" action="">

            <div class="shopinfol" style="font-size:14px">

                <h1 style="font-size:16px"><?php echo ($goods["title"]); ?></h1>

                <p style="font-size:16px;">

                    市场价:<font class="spirce"><del>￥<?php echo ($goods["original_price"]); ?></del></font>&nbsp;&nbsp;

                    <span class="vippfont">惊喜价:</span>

                    <span class="price">￥<?php echo ($goods["price"]); ?></span>

                </p>

                <!--商品宣传语--这里未使用-->
                <p class="gdesc" style="padding-top:5px; font-size:16px; font-weight:bold; padding-top:5px;">
                    <?php echo ($goods["slogan"]); ?>
                </p>	 

                <p style="height:24px; line-height:24px; padding-top:8px;">
                    <a class="gjian" style="cursor:pointer; display:block; 
                       float:left; width:35px; height:22px;line-height:22px;
                       text-align:center; font-size:18px; font-weight:bold; 
                       border:1px solid #e8e8e8; background:#ededed;border-radius:5px 0px 0px 5px">-</a>
                    <input readonly="" id="" name="number" value="1" class="inputBg" style="float:left;text-align: center; width:20px; height:22px; line-height:22px;border-bottom:1px solid #e8e8e8; border-top:1px solid #e8e8e8" type="text"> 
                    <a class="gjia" style="cursor:pointer; display:block;
                       float:left; width:35px; height:22px;line-height:22px;
                       text-align:center; font-size:18px; font-weight:bold; 
                       border:1px solid #e8e8e8; background:#ededed;border-radius:0px 5px 5px 0px">+</a>
                    <b style="margin-left:3px;"></b>
                </p>

                <div class="buyclass"> </div>

            </div>

        </form>

    </div>

    <div class="mainbottombg">

        <span class="ac" id="tab1" style="width:100%">产品详情</span>
        <!-- <span style="left:97px" id="tab2">用户评论</span>-->
    </div>

    <div style="padding:10px;" class="goods_desc">
        <?php echo (stripslashes(htmlspecialchars_decode($goods["detail"]))); ?>
    </div>

    <!--底部导航开始-->
    <div class="top_bar" style="-webkit-transform:translate3d(0,0,0)">

        <nav>

            <ul id="top_menu" class="top_menu">

                <li class="li1" style="width:20%">
                    <a href='<?php echo U("WeChat/Index/index");?>'>
                        <label>首页</label>
                    </a>
                </li>

                <li class="li2" style="width:30%">
                    <a id="btnCart"
                       data-href='<?php echo U("WeChat/Cart/add", array("id"=>$goods["id"],"ajax"=>1));?>'
                       class="butt-cart">
                        加入购物车
                    </a>
                </li>

                <li class="li4" style="width:30%">
                    <!-- <a id="btnBuy" href='<?php echo C("WEB_SITE_DOMAIN"); echo U("WeChat/Cart/add", array("id"=>$goods["id"],"nojump"=>1));?>' class="butt-buy">立即购买</a>-->
                    <a id="btnBuy" href='javascript:void(0)' class="butt-buy">立即购买</a>
                </li>

                <li class="li5" style="width:20%"><a href='<?php echo U("WeChat/Cart/index");?>' style="height:56px; padding:0px">
                        <span style="width:30px; height:32px; display:block; margin:0px auto">
                            <b>
                                <em id="buy_price" class="mycarts" value="1" style="display:block"><?php echo ($cart_count); ?></em>
                            </b>
                        </span>
                        <label>购物车</label>
                    </a>
                </li>   

            </ul>

        </nav>

    </div>
    <!--底部导航结束-->
</div>

<style type="text/css">
    #collectBox{width:100px;height:40px;z-index:-2;position:fixed;bottom:0px;right:0px;background:none;}
</style>
<div id="collectBox"></div>

<script>

    $(document).ready(function () {

        $("a.gjian").click(function () {

            gcount = $(this).parent().find("input[name=number]").val();

            if (gcount > 1) {
                gcount--;
                $(this).parent().find("input[name=number]").val(gcount)
            }

        });

        $("a.gjia").click(function () {

            gcount = $(this).parent().find("input[name=number]").val();

            gcount++;

            $(this).parent().find("input[name=number]").val(gcount)

        });

        //立即购买
        $("#btnBuy").click(function () {

            //var $url = '<?php echo C("WEB_SITE_DOMAIN"); echo U("WeChat/Cart/add", array("id"=>$goods["id"],"nojump"=>1));?>';
            var $url = '<?php echo U("WeChat/Cart/add", array("id"=>$goods["id"],"nojump"=>1));?>';

            var $gcount = $("input[name=number]").val();

            $url += "&gcount=" + $gcount;

            location.href = $url;
        });

        //加入购物车
        $("#btnCart").click(function () {

            var $url = $(this).attr("data-href");

            var $gcount = $("input[name=number]").val();

            $url += "&gcount=" + $gcount;

            $.getJSON($url, function (data) {

                if (data.info.status === 1) {

                    var flyElm = $('.ggimg').clone().css('opacity', '0.7');

                    flyElm.css({
                        'z-index': 9000,
                        'display': 'block',
                        'position': 'absolute',
                        'top': $('.ggimg').offset().top + 'px',
                        'left': $('.ggimg').offset().left + 'px',
                        'width': $('.ggimg').width() + 'px',
                        'height': $('.ggimg').height() + 'px'
                    });

                    $('body').append(flyElm);

                    hw = getPageSize();

                    flyElm.animate({
                        top: $('#collectBox').offset().top,
                        left: (hw[0] - 30) + 'px',
                        width: 30,
                        height: 30,
                    }, '2500', function () {
                        flyElm.animate({opacity: 'hide'}, 1000);
                    });

                    $cart_count = parseInt($("#buy_price").text());

                    $cart_count += parseInt($gcount);

                    $("#buy_price").text($cart_count);

                    //alert($cart_count);

                    jConfirm('进入购物车?', '下单结算', function (r) {
                        if (r)
                        {
                            window.location.href = "<?php echo U('Cart/index');?>";
                        }
                    });
                }
            });
        });
    });
</script>

<style type="text/css">

    body { padding-bottom:60px !important; }

    .top_menu li b {width: 38px;height: 20px;line-height: 17px;display: block;color: #fff;text-align: center;font-size: 12px;}

    .top_menu li b em {padding:0px 3px 0px 3px;border-radius: 100%;text-align: center;background-color: #32a000;display: block;position: absolute;z-index: 9999;margin-top: -10px;margin-left: 22px;}

    .user agent stylesheeti, cite, em, var, address, dfn {font-style: italic;}

    .top_menu li.li2 a.butt-cart{display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#32a000;}

    .top_menu li.li4 a.butt-buy {display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#ff6400;}
</style>

</body>

</html>