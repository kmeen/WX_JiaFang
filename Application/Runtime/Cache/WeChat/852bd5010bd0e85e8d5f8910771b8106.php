<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="cn">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title>
            <?php if(isset($meta_title)): echo ($meta_title); ?> - <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>
                <?php else: ?> 
                <?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN'); endif; ?>
        </title>

        <meta name="description" content=" <?php echo C('WEB_SITE_DESCRIPTION');?>" />

        <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>

        <script src="/Public/jquery/jquery-1.11.1.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <!-- basic styles -->
        <!--首页轮播和商品多图js-->
        <link rel="stylesheet" href="/Public/front/css/flexslider.css" />

        <link rel="stylesheet" href="/Public/front/css/comman.css" />

        <link rel="stylesheet" href="/Public/front/css/style.css" />

        <link rel="stylesheet" href="/Public/front/css/milk.css" />

        <link rel="stylesheet" href="/Public/front/css/user2015.css" />

        <link rel="stylesheet" href="/Public/front/css/tpl.css" />

        <link rel="stylesheet" href="/Public/front/css/style12.css" />

        <script src="/Public/front/js/jquery.min.js"></script> 

        <script src="/Public/front/js/jquery_dialog.js"></script>

        <!--会话框js和css-->
        <link rel="stylesheet" href="/Public/remind/css/alert.css" />

        <script src="/Public/remind/js/jquery.alerts.js"></script>
        <!--会话框js和css end-->

        <script src="/Public/front/js/jquery.flexslider-min.js"></script>

        <script src="/Public/front/js/common.js"></script>

        <script src="/Public/front/js/time.js"></script>

        <script src="/Public/front/js/user.js"></script>

        <!--调用微信js接口 2016-7-22 17:57:35-->
        <!--<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>-->

        <script type="text/javascript">
            //通过config接口注入权限验证配置
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。

                appId: '<?php echo ($jsappid); ?>', // 必填，公众号的唯一标识

                timestamp: '<?php echo ($jstimestamp); ?>', // 必填，生成签名的时间戳

                nonceStr: '<?php echo ($jsnonceStr); ?>', // 必填，生成签名的随机串

                signature: '<?php echo ($jsSign); ?>', // 必填，签名

                jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage"] // 必填，需要使用的JS接口列表
            });
            //通过ready接口处理成功验证
            wx.ready(function () {

                wx.onMenuShareTimeline({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Member/add_member_bankCard/flag/firstcash.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击分享到朋友圈');
                    },
                    success: function (res) {
                        //alert(res.title);
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }

                });

                wx.onMenuShareAppMessage({
                    title: "<?php echo C('WEB_SITE_TITLE');?> - <?php echo C('WEB_SITE_SLOGAN');?>", // 分享标题

                    link: "<?php echo C('WEB_SITE_DOMAIN');?>/index.php?s=/WeChat/Member/add_member_bankCard/flag/firstcash.html&fxidentify=<?php echo ($fxidentify); ?>", // 分享链接

                    imgUrl: "<?php echo C('WEB_SITE_DOMAIN'); echo get_logo(C('WEB_SITE_LOGO'));?>", // 分享图标

                    desc: "<?php echo C('WEB_SITE_DESCRIPTION');?>", // 分享描述

                    trigger: function (res) {
                        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                        //alert('用户点击发送给朋友');
                    },
                    success: function (res) {
                        //alert('已分享');
                    },
                    cancel: function (res) {
                        //alert('已取消');
                    },
                    fail: function (res) {
                        //alert(JSON.stringify(res));
                    }
                });

            });

            wx.error(function (res) {
                // alert(res);
                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

            });
        </script>

    </head>

    <body>


<div id="home">

    <div id="header">

        <div style="height:28px; padding-top:10px; background:url(/Public/Front/images/xy.png) 10px 8px no-repeat" class="logo"><span onclick=" history.go(-1);">&nbsp;</span></div>

        <div class="shoptitle"><span>添加银行卡</span></div>

        <div class="logoright" style="z-index: 1000">

    <div>

        <a onclick="ajax_show_menu()" href="javascript:;"></a>

        <div class="showmenu">

            <p>
                <a href="<?php echo U('WeChat/Index/index');?>">返回首页</a>
            </p>

            <p>
                <a href="<?php echo U('WeChat/Member/center');?>">会员中心</a>
            </p>

            <p>
                <a href="<?php echo U('WeChat/Member/order');?>">我的订单</a>
            </p>

            <p>
                <a href="<?php echo U('WeChat/Cart/index');?>">购&nbsp;物&nbsp;车</a>
            </p>

            <p style="border:none">
                <a href="javascript:;" onclick="window.location.reload();">刷新页面</a>
            </p>

        </div>

    </div>

</div>

<script type="text/javascript">
    function ajax_show_menu() {
        $(".showmenu").toggle(500);
    }
</script>

    </div>

</div>

<div style="display: none;" id="opquyu"></div>

<div id="opquyubox" style="display:none">
    
    <div style="line-height:26px;">
        
        <?php if(!empty($category)): if(is_array($category)): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "暂时没有数据" ;else: foreach($__LIST__ as $key=>$c): $mod = ($i % 2 );++$i;?><h2 style="border-bottom:1px solid #ededed;"><a href="<?php echo U('WeChat/Category/index',array('id'=>$c['id']));?>"><?php echo ($c["title"]); ?></a></h2><?php endforeach; endif; else: echo "暂时没有数据" ;endif; endif; ?>
        
    </div>
    
    <div style=" height:45px;"></div>
    
</div>

<!--<script type="text/javascript">
    
    $(document).ready(function ($) {
        
        $('#opquyu').click(function () {
            
            $(this).hide();
            
            $("#opquyubox").hide();
            
        });

    });
    
</script>-->

<style type="text/css">

    .pw,.pwt{
        height:32px; line-height:normal;
        border: 1px solid #ddd;
        border-radius: 5px;
        background-color: #fff; padding:0 5px;
        -moz-border-radius:5px;/*仅Firefox支持，实现圆角效果*/
        -webkit-border-radius:5px;/*仅Safari,Chrome支持，实现圆角效果*/
        -khtml-border-radius:5px;/*仅Safari,Chrome支持，实现圆角效果*/
        border-radius:5px;/*仅Opera，Safari,Chrome支持，实现圆角效果*/
        font-size:12px
    }

    .pw{ width:90%;}

    table td{font-size:14px}

</style>


<div id="main" style="">

    <div style="padding:10px 0; background:#f5f5f5; overflow:hidden;">

        <form name="form_addbank" id="form_addbank" action="" method="post">

            <input type="hidden"  name="user_id" value='<?php echo ($user_id); ?>'  class="pw"/>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="line-height:40px;">

                <tr>
                    <td width="25%" align="right" style="padding-bottom:2px;"><b class="cr2">*</b> 开户银行：</td>
                    <td width="75%" align="left" style="padding-bottom:2px;">
                        <input type="text" value="" placeholder="开户行"  name="bank_name"  class="pw"/>
                    </td>
                </tr>

                <tr>
                    <td width="25%" align="right" style="padding-bottom:2px;"><b class="cr2">*</b> 支&emsp;&emsp;行：</td>
                    <td width="75%" align="left" style="padding-bottom:2px;">
                        <input type="text" value="" placeholder="所在省" name="province" style="width:20%" class="pw"/>
                        <input type="text" value="" placeholder="所在城市" name="city"  style="width:20%" class="pw"/>
                        <input type="text" value="" placeholder="支行名" name="branch"  style="width:30%" class="pw"/>
                    </td>
                </tr>

                <tr>
                    <td width="25%" align="right" style="padding-bottom:2px;"><b class="cr2">*</b> 真实姓名：</td>
                    <td width="75%" align="left" style="padding-bottom:2px;">
                        <input type="text" value="" placeholder="真实姓名" name="account_name"  class="pw"/></td>
                </tr>

                <tr>
                    <td width="25%" align="right" style="padding-bottom:2px;"><b class="cr2">*</b> 银行卡号：</td>
                    <td width="75%" align="left" style="padding-bottom:2px;">
                        <input id="account_bank" type="text" value=""  placeholder="银行卡号"  name="account_bank"  class="pw"/>
                    </td>

                <script type="text/javascript">

                    window.onload = function () {

                        document.getElementById("account_bank").onkeyup = function () {

                            this.value = this.value.replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1 ");

                        };

                    };

                </script>
                </tr>

                <tr>
                    <td width="25%" align="right" style="padding-bottom:2px;"><b class="cr2">*</b> 手机号码：</td>
                    <td width="75%" align="left" style="padding-bottom:2px;">
                        <input type="text" value=""  placeholder="手机号码"  name="phone"  class="pw"/>
                    </td>
                </tr>

                <!--                <tr>
                                    <td width="25%" align="right" style="padding-bottom:2px;"><b class="cr2">*</b> 设置默认：</td>
                                    <td width="75%" align="left" style="padding-bottom:2px;">
                                        <input type="radio" name='is_default' value="0"  checked /> 否
                                        
                                        <input type="radio" name='is_default' value="1"  /> 是
                                    </td>
                                </tr>-->

                <tr>
                    <td align="center" style="padding-top:10px;" colspan="2">
                        <a href="javascript:;" onclick="$('#form_addbank').submit();" style="position:fixed;bottom:57px;max-width: 620px;width:100%; border-radius:5px;display:block;background:#DB383E;cursor:pointer; height:40px; line-height:40px; font-size:14px; color:#FFF">确认提交</a>
                    </td>
                </tr>

            </table>

        </form>

    </div>

</div>

<div style="-webkit-transform:translate3d(0,0,0)" class="top_bar">
    <nav>
        <ul class="top_menu" id="top_menu">
            <li class="li1">
                <a  href="<?php echo U('Index/index');?>">
                    <label>首页</label>
                </a>
            </li>
            <li class="li2">
                <a  onclick="ajaxopquyu()">
                    <label>分类</label>

                </a>
            </li>
            <li class="li4">
                <a  href="<?php echo U('WeChat/Member/center');?>">
                    <label>会员</label>
                </a>
            </li>
            <li class="li5">
                <a style="height:56px; padding:0px"  href="<?php echo U('WeChat/Cart/index');?>">
                    <span style="width:30px; height:32px; display:block; margin:0px auto">
                        <b>
                            <em style="display:block" value="1" class="mycarts" id="buy_price"><?php echo ($cart_count); ?></em>
                        </b>
                    </span>
                    <label>购物车</label>
                </a>
            </li>    
        </ul>
    </nav>
</div>

<style type="text/css">
    body { padding-bottom:60px !important; }
    .top_menu li b {width: 38px;height: 20px;line-height: 17px;display: block;color: #fff;text-align: center;font-size: 12px;}
    .top_menu li b em {padding:0px 3px 0px 3px;border-radius: 100%;text-align: center;background-color: #32a000;display: block;position: absolute;z-index: 9999;margin-top: -10px;margin-left: 22px;}
    user agent stylesheeti, cite, em, var, address, dfn {font-style: italic;}

    .top_menu li.li2 a.butt-cart{display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#32a000;}
    .top_menu li.li4 a.butt-buy {display: inline-block;font-size: 15px;width: 90%;height: 40px;line-height: 38px;margin: 6px auto 5px auto;padding: 0;color: #FFF;border-radius: 3px;background:#ff6400;}
</style>
</body>
</html>