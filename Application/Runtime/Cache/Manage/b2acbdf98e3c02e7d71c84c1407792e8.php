<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta name="author" content="<?php echo C('WEB_SITE_TITLE');?>">
    <meta name="keywords" content="<?php echo ($meta_keywords); ?>">
    <meta name="description" content="<?php echo ($meta_description); ?>">
    <meta name="generator" content="KMEEN">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php echo C('WEB_SITE_TITLE');?>">
    <meta name="format-detection" content="telephone=no,email=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo ($meta_title); ?>｜<?php echo C('WEB_SITE_TITLE');?>后台管理</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/Public/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/bootstrapextend/css/bootstrap.extend.css">

<script type="text/javascript" src="/Public/jquery/jquery-1.11.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/jquery/jquery.cookie.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/bootstrapextend/js/bootstrap.extend.js" charset="utf-8"></script>
<style rel="stylesheet" type="text/css">
    @media (min-width: 768px){
        .wrap>.left{
            padding: 0px;
            top: 52px;
            bottom: 0px;
            position: absolute;
            overflow: auto;
            background: rgb(245, 246, 247);
        }
        .wrap>.right{
            top: 52px;
            right: 0px;
            bottom: 0px;
            position: absolute;
            overflow: auto;
        }
    }
    @media (max-width: 768px){
        .wrap>.left{
            padding: 0px;
        }
    }
</style>

</head>
<body>
    <nav class="navbar navbar-inverse margin-none border-radius-none" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-top">
                    <span class="sr-only">切换导航</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if(C('WEB_SITE_LOGO')): ?>
                <a class="navbar-brand" target="_blank" href="/"><img style="height: 40px;margin-top: -10px;" class="logo" src="<?php echo (get_cover(C("WEB_SITE_LOGO"),'path')); ?>"></a>
                <?php else: ?>
                <a class="navbar-brand" target="_blank" href="/"><?php echo C('WEB_SITE_TITLE');?></a>
                <?php endif; ?>
            </div>
            <div class="collapse navbar-collapse navbar-collapse-top">
                <!-- 顶部主导航 -->
                <ul class="nav navbar-nav">
                    <?php if(is_array($__ALLMENULIST__)): $i = 0; $__LIST__ = $__ALLMENULIST__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if($vo['id'] == $__CURRENT_ROOTMENU__) echo 'class="active"'; ?> >
                        <a href="<?php echo U($vo['url']);?>"><i class="<?php echo ($vo["icon"]); ?>"></i> <?php echo ($vo["title"]); ?></a>
                        </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo U('Index/rmdirr');?>" class="ajax-get">
                            <i class="glyphicon glyphicon-trash"></i> 清空缓存
                        </a>
                    </li>
                    <!--                    <li>
                                            <a target="_blank" href="/">
                                                <i class="glyphicon glyphicon-new-window"></i> 打开前台
                                            </a>
                                        </li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i> <?php echo ($__USER__["username"]); ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <!--                            <li>
                                                            <a target="_blank" href="<?php echo U('Home/User/center');?>">
                                                                <i class="glyphicon glyphicon-home"></i> 个人中心
                                                            </a>
                                                        </li>-->
                            <li>
                                <a href="<?php echo U('User/edit', array('id' => is_login()));?>">
                                    <i class="glyphicon glyphicon-edit"></i> 修改信息
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo U('Public/logout');?>" class="ajax-get">
                                    <i class="glyphicon glyphicon-log-out"></i> 退出
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrap">
        
            <div class="col-xs-12 col-sm-2 left">
                <!-- 侧边导航 -->
                <div class="panel-group margin-none" role="tablist">
                    <?php $__SIDEMENU__ = $__ALLMENULIST__[$__CURRENT_ROOTMENU__]['_child']; ?>
                    <?php if(is_array($__SIDEMENU__)): $i = 0; $__LIST__ = $__SIDEMENU__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="panel panel-default margin-none border-radius-none border-bottom-none">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#side-menu<?php echo ($key); ?>">
                                        <i class="<?php echo ($vo["icon"]); ?>"></i> <?php echo ($vo["title"]); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="side-menu<?php echo ($key); ?>" class="panel-collapse collapse in" role="tabpanel">
                                <div class="list-group">
                                    <?php if(is_array($vo["_child"])): $i = 0; $__LIST__ = $vo["_child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo_child): $mod = ($i % 2 );++$i;?><a class="list-group-item border-radius-none <?php if(in_array($vo_child['id'], $__PARENT_MENU_ID__)) echo 'active'; ?>"
                                           href="<?php echo U($vo_child['url']);?>">
                                            <i class="<?php echo ($vo_child["icon"]); ?>"></i> <?php echo ($vo_child["title"]); ?>
                                        </a><?php endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                            </div>
                        </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10 right">
                <ul class="breadcrumb margin-bottom border-radius-none" style="margin-left:-15px;margin-right:-15px;">
                    <li><i class="icon-location-arrow"></i></li>
                    <?php if(is_array($__PARENT_MENU__)): $i = 0; $__LIST__ = $__PARENT_MENU__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                            <a href="<?php echo U($vo['url']);?>">
                                <?php echo ($vo["title"]); ?>
                            </a>
                        </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                
    <div class="builder <?php echo ($builder_class); ?>">
        <div class="row">
            <?php if(!empty($tab_list)): ?><div class="col-xs-12">
                    <ul class="nav nav-tabs">
                        <?php if(is_array($tab_list)): $i = 0; $__LIST__ = $tab_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tab): $mod = ($i % 2 );++$i;?><li class="<?php if($current_tab == $key) echo 'active'; ?>"><a href="<?php echo U($tab_url, array('tab' => $key));?>"><?php echo ($tab); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                    <div class="blank"></div>
                </div><?php endif; ?>
            <?php if(!empty($button_list)): ?><div class="col-xs-12 col-sm-9 margin-bottom">
                    <?php if(is_array($button_list)): $i = 0; $__LIST__ = $button_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$button): $mod = ($i % 2 );++$i;?><a style="margin-right:5px;" <?php echo ($button["attr"]); ?>>89999<?php echo ($button["title"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
                </div><?php endif; ?>
            <?php if(!empty($search)): ?><div class="col-xs-12 col-sm-3 margin-bottom">
                    <div class="input-group search-form">
                        <input type="text" name="keyword" class="search-input form-control" value="<?php echo ($_GET["keyword"]); ?>" placeholder="<?php echo ($search["title"]); ?>">
                        <span class="input-group-btn"><a class="btn btn-default" href="javascript:;" id="search" url="<?php echo ($search["url"]); ?>"><i class="glyphicon glyphicon-search"></i></a></span>
                    </div>
                </div><?php endif; ?>

            <!-- 数据列表 -->
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><input class="check-all" type="checkbox"></th>
                                <th>编号</th>
                                <th>订单号</th>
                                <th>商品</th>
                                <th>支付方式</th>
                                <th>总金额</th>
                                <th>收货人</th>
                                <th>收货人手机</th>
                                <th>收货地址</th>
                                <th>下单时间</th>
                                <th style="text-align: center;">状态</th>
                                <th style="text-align: center;">操作</th>  
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($data_list)): $i = 0; $__LIST__ = $data_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><tr>
                                <td><input class="ids" type="checkbox" value="<?php echo ($data['id']); ?>" name="ids[]"></td>
                                <td><?php echo ($data["id"]); ?></td>
                                <td><?php echo ($data["order_no"]); ?></td>
                                <td><?php echo (check_goods_title($data["ids"])); ?></td>
                                <td><?php echo ($data["pay_name"]); ?></td>
                                <td><?php echo ($data["total"]); ?></td>
                                <td><?php echo ($data["name"]); ?></td>
                                <td><?php echo ($data["phone"]); ?></td>
                                <td><?php echo ($data["province"]); echo ($data["city"]); echo ($data["area"]); echo ($data["address"]); ?></td>
                                <td><?php echo (date("Y-m-d H:i",$data["create_time"])); ?></td>
                                <td style="text-align: center;">
                                    <?php echo get_order_status($data['pay_status'],$data['send_status'],$data['status']);?>
                                </td>
                                <td style="text-align: center;">
                            <?php if($data["status"] == 1): ?><a class="ajax-get confirm" href="<?php echo U('Order/cancel',array('id'=>$data['id']));?>">取消</a><?php endif; ?>
                            <?php if($data["status"] == 4): ?><a href="<?php echo U('Order/refund',array('id'=>$data['id']));?>" class="confirm">退货</a><?php endif; ?>
                            <?php if(($data["send_status"] == 0) and ($data["pay_status"] >= 2)): ?><a href="<?php echo U('Order/sendOut',array('id'=>$data['id']));?>">发货</a><?php endif; ?>
                            <?php if(($data["send_status"] == 2) and ($data["pay_status"] >= 2) and ($data["commission_status"] == 0)): ?><a class="ajax-get confirm" href="<?php echo U('Order/giveOut',array('id'=>$data['id']));?>">放佣</a><?php endif; ?>
                            <a href="<?php echo U('Order/detail',array('id'=>$data['id']));?>">详情</a>

                            </td>

                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                        </tbody>
                    </table>
                    <ul class="pagination"><?php if(!empty($page)): echo ($page); endif; ?></ul>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //搜索功能
        $("#search").click(function () {
            var url = $(this).attr('url');
            var query = $('.search-form').find('input').serialize();
            query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g, '');
            query = query.replace(/^&/g, '');
            if (url.indexOf('?') > 0) {
                url += '&' + query;
            } else {
                url += '?' + query;
            }
            window.location.href = url;
        });

        //回车搜索
        $(".search-input").keyup(function (e) {
            if (e.keyCode === 13) {
                $("#search").click();
                return false;
            }
        });
    </script>

            </div>
        
    </div>
</body>
</html>