<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta name="author" content="<?php echo C('WEB_SITE_TITLE');?>">
    <meta name="keywords" content="<?php echo ($meta_keywords); ?>">
    <meta name="description" content="<?php echo ($meta_description); ?>">
    <meta name="generator" content="KMEEN">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php echo C('WEB_SITE_TITLE');?>">
    <meta name="format-detection" content="telephone=no,email=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo ($meta_title); ?>｜<?php echo C('WEB_SITE_TITLE');?>后台管理</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/Public/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/bootstrapextend/css/bootstrap.extend.css">

    <style type="text/css">
        body {
            background-color: #f6f6f6;
        }
    </style>

<script type="text/javascript" src="/Public/jquery/jquery-1.11.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/jquery/jquery.cookie.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/bootstrapextend/js/bootstrap.extend.js" charset="utf-8"></script>
<style rel="stylesheet" type="text/css">
    @media (min-width: 768px){
        .wrap>.left{
            padding: 0px;
            top: 52px;
            bottom: 0px;
            position: absolute;
            overflow: auto;
            background: rgb(245, 246, 247);
        }
        .wrap>.right{
            top: 52px;
            right: 0px;
            bottom: 0px;
            position: absolute;
            overflow: auto;
        }
    }
    @media (max-width: 768px){
        .wrap>.left{
            padding: 0px;
        }
    }
</style>

    <script type="text/javascript">
        $(function(){
        //用户增长曲线图
        var mychart = new Chart($("#mychart").get(0).getContext("2d")).Line(
        {
        labels : <?php echo ($user_reg_date); ?>,
                datasets : [
                {
                fillColor : "#337ab7",
                        strokeColor : "#337ab7",
                        pointColor : "#337ab7",
                        pointStrokeColor : "#fff",
                        data : <?php echo ($user_reg_count); ?>
                }
                ]
        },
        {
        scaleLineColor : "rgba(0,0,0,.1)", //X/Y轴的颜色
                scaleLineWidth : 1 //X/Y轴的宽度
        }
        );
                //图表时间段选择
                $('#daterange_set').daterangepicker({
        startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2015',
                maxDate: '12/31/2100',
                dateLimit: { days: 360 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                '最近7天': [moment().subtract(6, 'days'), moment()],
                        '这个月': [moment().startOf('month'), moment().endOf('month')],
                        '上个月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-sm btn-primary',
                cancelClass: 'btn-sm',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                applyLabel: '确定',
                        cancelLabel: '清除',
                        fromLabel: '从',
                        toLabel: '到',
                        customRangeLabel: '自定义',
                        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
                        firstDay: 1
                }
        },
                function(start, end, label){
                var url = '<?php echo U("Index/index");?>';
                        var query = 'start_date=' + start + '&end_date=' + end;
                        if (url.indexOf('?') > 0){
                url += '&' + query;
                } else{
                url += '?' + query;
                }
                window.location.href = url;
                }
        );
        });
    </script>

</head>
<body>
    <nav class="navbar navbar-inverse margin-none border-radius-none" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-top">
                    <span class="sr-only">切换导航</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if(C('WEB_SITE_LOGO')): ?>
                <a class="navbar-brand" target="_blank" href="/"><img style="height: 40px;margin-top: -10px;" class="logo" src="<?php echo (get_cover(C("WEB_SITE_LOGO"),'path')); ?>"></a>
                <?php else: ?>
                <a class="navbar-brand" target="_blank" href="/"><?php echo C('WEB_SITE_TITLE');?></a>
                <?php endif; ?>
            </div>
            <div class="collapse navbar-collapse navbar-collapse-top">
                <!-- 顶部主导航 -->
                <ul class="nav navbar-nav">
                    <?php if(is_array($__ALLMENULIST__)): $i = 0; $__LIST__ = $__ALLMENULIST__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if($vo['id'] == $__CURRENT_ROOTMENU__) echo 'class="active"'; ?> >
                        <a href="<?php echo U($vo['url']);?>"><i class="<?php echo ($vo["icon"]); ?>"></i> <?php echo ($vo["title"]); ?></a>
                        </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo U('Index/rmdirr');?>" class="ajax-get">
                            <i class="glyphicon glyphicon-trash"></i> 清空缓存
                        </a>
                    </li>
                    <!--                    <li>
                                            <a target="_blank" href="/">
                                                <i class="glyphicon glyphicon-new-window"></i> 打开前台
                                            </a>
                                        </li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i> <?php echo ($__USER__["username"]); ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <!--                            <li>
                                                            <a target="_blank" href="<?php echo U('Home/User/center');?>">
                                                                <i class="glyphicon glyphicon-home"></i> 个人中心
                                                            </a>
                                                        </li>-->
                            <li>
                                <a href="<?php echo U('User/edit', array('id' => is_login()));?>">
                                    <i class="glyphicon glyphicon-edit"></i> 修改信息
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo U('Public/logout');?>" class="ajax-get">
                                    <i class="glyphicon glyphicon-log-out"></i> 退出
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrap">
        
    <div class="container">
        <div class="dashboard margin-top">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-actions"></div>
                            <i class="glyphicon glyphicon-home"></i> 系统概览
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td>今日增加订单数</td>
                                        <td>
                                            <a class="btn btn-warning btn-xs" href="<?php echo U('Manage/Order/index');?>"><?php echo ((isset($today_order_count) && ($today_order_count !== ""))?($today_order_count):'0'); ?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>今日增加会员数</td>
                                        <td>
                                            <a class="btn btn-warning btn-xs" href="<?php echo U('Manage/Member/index');?>"><?php echo ((isset($today_member_count) && ($today_member_count !== ""))?($today_member_count):'0'); ?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>总订单数</td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php echo U('Manage/Order/index');?>"><?php echo ((isset($all_order_count) && ($all_order_count !== ""))?($all_order_count):'0'); ?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>总会员数</td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php echo U('Manage/Member/index');?>"><?php echo ((isset($all_member_count) && ($all_member_count !== ""))?($all_member_count):'0'); ?></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-actions"></div>
                            <i class="glyphicon glyphicon-cog"></i> 系统信息
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td>服务器操作系统</td>
                                        <td><?php echo (PHP_OS); ?></td>
                                    </tr>
                                    <tr>
                                        <td>运行环境</td>
                                        <td><?php echo ($_SERVER['SERVER_SOFTWARE']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>MYSQL版本</td>
                                        <td><?php $system_info_mysql = M()->query("select version() as v;"); echo ($system_info_mysql["0"]["v"]); ?></td>
                                </tr>
                                <tr>
                                    <td>上传限制</td>
                                    <td><?php echo ini_get('upload_max_filesize');?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-actions"></div>
                            <i class="glyphicon glyphicon-th-list"></i> 产品团队
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td>总策划</td>
                                        <td>郑州仟米软件科技有限公司</td>
                                    </tr>
                                    <tr>
                                        <td>研发团队</td>
                                        <td>研发部</td>
                                    </tr>
                                    <tr>
                                        <td>官方网址</td>
                                        <td><a target="blank" href="http://www.kmeen.com">http://www.kmeen.com</a></td>
                                    </tr>
                                    <tr>
                                        <td>联系我们</td>
                                        <td>350687161@qq.com</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <button id="daterange_set" class="btn btn-default btn-sm" style="margin-top:-5px;margin-right:-5px;"><span class="glyphicon glyphicon-cog"></span></button>
                            </div>
                            <i class="glyphicon glyphicon-stats"></i> 用户增长统计
                        </div>
                        <div class="panel-body">
                            <h5 class="text-center"><?php echo ($start_date); ?>－<?php echo ($end_date); ?> <?php echo ($count_day); ?>天用户增长</h5>
                            <canvas id="mychart" style="width:100%;height:300px;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</body>
</html>