<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta name="author" content="<?php echo C('WEB_SITE_TITLE');?>">
    <meta name="keywords" content="<?php echo ($meta_keywords); ?>">
    <meta name="description" content="<?php echo ($meta_description); ?>">
    <meta name="generator" content="KMEEN">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php echo C('WEB_SITE_TITLE');?>">
    <meta name="format-detection" content="telephone=no,email=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo ($meta_title); ?>｜<?php echo C('WEB_SITE_TITLE');?>后台管理</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/Public/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/bootstrapextend/css/bootstrap.extend.css">

    <style type="text/css">
        body {
            background-color: #f6f6f6;
        }
    </style>

<script type="text/javascript" src="/Public/jquery/jquery-1.11.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/jquery/jquery.cookie.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
<script type="text/javascript" src="/Public/bootstrapextend/js/bootstrap.extend.js" charset="utf-8"></script>
<style rel="stylesheet" type="text/css">
    @media (min-width: 768px){
        .wrap>.left{
            padding: 0px;
            top: 52px;
            bottom: 0px;
            position: absolute;
            overflow: auto;
            background: rgb(245, 246, 247);
        }
        .wrap>.right{
            top: 52px;
            right: 0px;
            bottom: 0px;
            position: absolute;
            overflow: auto;
        }
    }
    @media (max-width: 768px){
        .wrap>.left{
            padding: 0px;
        }
    }
</style>

    <script type="text/javascript">
        $(function(){
        //订单增长曲线图
        var mychart = new Chart($("#mychart").get(0).getContext("2d")).Line(
        {
        labels : <?php echo ($user_reg_date); ?>,
                datasets : [
                {
                fillColor : "#337ab7",
                        strokeColor : "#337ab7",
                        pointColor : "#337ab7",
                        pointStrokeColor : "#fff",
                        data : <?php echo ($user_reg_count); ?>
                }
                ]
        },
        {
        scaleLineColor : "rgba(0,0,0,.1)", //X/Y轴的颜色
                scaleLineWidth : 1 //X/Y轴的宽度
        }
        );
        //图表时间段选择
        $('#daterange_set').daterangepicker({
        startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2015',
                maxDate: '12/31/2100',
                dateLimit: { days: 360 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                '最近7天': [moment().subtract(6, 'days'), moment()],
                        '这个月': [moment().startOf('month'), moment().endOf('month')],
                        '上个月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-sm btn-primary',
                cancelClass: 'btn-sm',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                applyLabel: '确定',
                        cancelLabel: '清除',
                        fromLabel: '从',
                        toLabel: '到',
                        customRangeLabel: '自定义',
                        daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
                        firstDay: 1
                }
        },
                function(start, end, label){
                var url = '<?php echo ($burl); ?>';
                var query = 'start_date=' + start + '&end_date=' + end;
                if (url.indexOf('?') > 0){
                url += '&' + query;
                } else{
                url += '?' + query;
                }
                window.location.href = url;
                }
        );
        });
    </script>

</head>
<body>
    <nav class="navbar navbar-inverse margin-none border-radius-none" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-top">
                    <span class="sr-only">切换导航</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if(C('WEB_SITE_LOGO')): ?>
                <a class="navbar-brand" target="_blank" href="/"><img style="height: 40px;margin-top: -10px;" class="logo" src="<?php echo (get_cover(C("WEB_SITE_LOGO"),'path')); ?>"></a>
                <?php else: ?>
                <a class="navbar-brand" target="_blank" href="/"><?php echo C('WEB_SITE_TITLE');?></a>
                <?php endif; ?>
            </div>
            <div class="collapse navbar-collapse navbar-collapse-top">
                <!-- 顶部主导航 -->
                <ul class="nav navbar-nav">
                    <?php if(is_array($__ALLMENULIST__)): $i = 0; $__LIST__ = $__ALLMENULIST__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li <?php if($vo['id'] == $__CURRENT_ROOTMENU__) echo 'class="active"'; ?> >
                        <a href="<?php echo U($vo['url']);?>"><i class="<?php echo ($vo["icon"]); ?>"></i> <?php echo ($vo["title"]); ?></a>
                        </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo U('Index/rmdirr');?>" class="ajax-get">
                            <i class="glyphicon glyphicon-trash"></i> 清空缓存
                        </a>
                    </li>
                    <!--                    <li>
                                            <a target="_blank" href="/">
                                                <i class="glyphicon glyphicon-new-window"></i> 打开前台
                                            </a>
                                        </li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i> <?php echo ($__USER__["username"]); ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <!--                            <li>
                                                            <a target="_blank" href="<?php echo U('Home/User/center');?>">
                                                                <i class="glyphicon glyphicon-home"></i> 个人中心
                                                            </a>
                                                        </li>-->
                            <li>
                                <a href="<?php echo U('User/edit', array('id' => is_login()));?>">
                                    <i class="glyphicon glyphicon-edit"></i> 修改信息
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo U('Public/logout');?>" class="ajax-get">
                                    <i class="glyphicon glyphicon-log-out"></i> 退出
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrap">
        
            <div class="col-xs-12 col-sm-2 left">
                <!-- 侧边导航 -->
                <div class="panel-group margin-none" role="tablist">
                    <?php $__SIDEMENU__ = $__ALLMENULIST__[$__CURRENT_ROOTMENU__]['_child']; ?>
                    <?php if(is_array($__SIDEMENU__)): $i = 0; $__LIST__ = $__SIDEMENU__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="panel panel-default margin-none border-radius-none border-bottom-none">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#side-menu<?php echo ($key); ?>">
                                        <i class="<?php echo ($vo["icon"]); ?>"></i> <?php echo ($vo["title"]); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="side-menu<?php echo ($key); ?>" class="panel-collapse collapse in" role="tabpanel">
                                <div class="list-group">
                                    <?php if(is_array($vo["_child"])): $i = 0; $__LIST__ = $vo["_child"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo_child): $mod = ($i % 2 );++$i;?><a class="list-group-item border-radius-none <?php if(in_array($vo_child['id'], $__PARENT_MENU_ID__)) echo 'active'; ?>"
                                           href="<?php echo U($vo_child['url']);?>">
                                            <i class="<?php echo ($vo_child["icon"]); ?>"></i> <?php echo ($vo_child["title"]); ?>
                                        </a><?php endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                            </div>
                        </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10 right">
                <ul class="breadcrumb margin-bottom border-radius-none" style="margin-left:-15px;margin-right:-15px;">
                    <li><i class="icon-location-arrow"></i></li>
                    <?php if(is_array($__PARENT_MENU__)): $i = 0; $__LIST__ = $__PARENT_MENU__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                            <a href="<?php echo U($vo['url']);?>">
                                <?php echo ($vo["title"]); ?>
                            </a>
                        </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-12">

            <?php if($CONTROLLER_NAME0 == 'Order'): ?><div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right">
                            <button id="daterange_set" class="btn btn-default btn-sm" style="margin-top:-5px;margin-right:-5px;"><span class="glyphicon glyphicon-cog"></span></button>
                        </div>
                        <i class="glyphicon glyphicon-stats"></i> <?php echo ($statistics_title); ?>增长统计 &nbsp;&nbsp;

                        <b>今日支付订单数:</b><span style='color:red; font-size:18px'><?php echo ($o_today_num); ?></span>&nbsp;&nbsp;<b>今日进账:</b><span style='color:red; font-size:18px'>￥<?php echo ((isset($o_today_spend) && ($o_today_spend !== ""))?($o_today_spend): '0.00'); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>05月支付订单数:</b><span style='color:red; font-size:18px'><?php echo ($o_moth_num); ?></span>&nbsp;&nbsp;<b>05月进账:</b><span style='color:red; font-size:18px'>￥<?php echo ((isset($o_moth_spend) && ($o_moth_spend !== ""))?($o_moth_spend): '0.00'); ?></span>  


                    </div>
                    <div class="panel-body">
                        <h5 class="text-center"><?php echo ($start_date); ?>－<?php echo ($end_date); ?> <?php echo ($count_day); ?>天订单增长</h5>
                        <canvas id="mychart" style="width:100%;height:300px;"></canvas>
                    </div>
                </div><?php endif; ?>
            
             <?php if($CONTROLLER_NAME0 == 'Member'): ?><div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right">
                            <button id="daterange_set" class="btn btn-default btn-sm" style="margin-top:-5px;margin-right:-5px;"><span class="glyphicon glyphicon-cog"></span></button>
                        </div>
                        <i class="glyphicon glyphicon-stats"></i> <?php echo ($statistics_title); ?>增长统计 &nbsp;&nbsp;

                    </div>
                    <div class="panel-body">
                        <h5 class="text-center"><?php echo ($start_date); ?>－<?php echo ($end_date); ?> <?php echo ($count_day); ?>天会员增长</h5>
                        <canvas id="mychart" style="width:100%;height:300px;"></canvas>
                    </div>
                </div><?php endif; ?>
            
        </div>
    </div>

            </div>
        
    </div>
</body>
</html>