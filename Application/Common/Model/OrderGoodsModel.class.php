<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 订单模型
 * @author xzake
 */
class OrderGoodsModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('name', 'require', '收货人不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('phone', 'require', '收货人手机不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('province', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('city', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('area', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('address', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
//        array('create_time', NOW_TIME, self::MODEL_INSERT),
//        array('update_time', NOW_TIME, self::MODEL_BOTH),
//        array('status', '1', self::MODEL_INSERT),
    );

    /*
     * 根据订单id 获取订单商品信息
     * 
     * @param id
     * @str_goods_title TRUE 获取订单商品的商品名称
     * 
     * @return status 执行状态(FALSE 查询异常 1正确处理)  msg 执行结果(status FALSE时异常信息提示，1正确处理结果)
     */

    public function get_ordergoods($order_id, $str_goods_title = FALSE) {

        if (empty($order_id)) {

            return array('status' => FALSE, 'msg' => '参数丢失！');
        }

        $order_goods = $this->where('order_id =' . $order_id)->select();

        if (empty($order_goods)) {

            return array('status' => FALSE, 'msg' => '没有查找到订单商品！');
        }

        #获取订单商品的商品名称
        if ($str_goods_title) {

            foreach ($order_goods as $v) {

                $goods_title[] = $v['title'];

                return array('status' => 1, 'msg' => implode(',', $goods_title));
            }
        }

        return array('status' => 1, 'msg' => $order_goods);
    }

}
