<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 分类模型
 * @author xzake
 */
class WechatModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
        array('wechat_name', 'require', '微信名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('wechat_id', 'require', '原始ID不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('wechat', 'require', '微信号不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('token', 'require', 'token不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('appid', 'require', 'AppId不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('secret', 'require', 'AppSecret不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('mch_id', 'require', '微信商户号不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('paykey', 'require', '商户密钥不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
    );

}
