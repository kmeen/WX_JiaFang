<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 分类模型
 * @author xzake
 */
class CustomReplyMultiModel extends Model {
    /**
     * 自动验证规则
     * @author xzake
     */

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
        array('keyword', 'require', '关键字不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('keyword', '1,32', '关键字长度为1-32个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
        array('keyword', '', '关键字已经存在', self::VALUE_VALIDATE, 'unique', self::MODEL_BOTH),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
    );

}
