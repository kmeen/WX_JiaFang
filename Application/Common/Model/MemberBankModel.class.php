<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 会员银行卡模型
 * @author xingji
 */
class MemberBankModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('title', 'require', '名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('title', '1,100', '名称长度为1-100个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
//        array('title', 'checkTitle', '名称已经存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '0', self::MODEL_INSERT),
    );

    /*
     * 查询会员银行卡信息
     * 
     * @param int member_id 会员编号
     * 
     * @param tinyint is_default FALSE 所有银行卡  TRUE 默认银行卡
     * 
     * @param int card_id 会员银行卡唯一标识 id
     */

    public function get_member_bankcard($member_id, $is_default = FALSE, $card_id) {

        $member_bank_data = '';

        if ($is_default) {

            $map['is_default'] = 1;
        }

        if ($card_id) {

            $exit_card = $this->find($card_id);

            if (empty($exit_card)) {

                return array('status' => false, 'msg' => '没有找到您要编辑的银行卡！');
            } else {

                $map['id'] = $card_id;
            }
        }

        $map['user_id'] = $member_id;

       
        if ($is_default) {
            

            $member_bank_data = $this->where($map)->find();
        } else {
            

            if ($card_id) {
                
                $member_bank_data = $this->where($map)->find();
            } else {

                $member_bank_data = $this->where($map)->order('is_default desc')->select();
            }
        }

        return array('status' => 1, 'msg' => $member_bank_data);
    }

    /*
     * 处理会员银行卡
     * 
     * @param  string type 处理方式 del 删除操作
     * 
     * @param  int card_id 银行卡唯一标识
     */

    public function handle_member_bankcard($type, $card_id) {

        if (!is_string($type) || empty($card_id)) {

            return array('status' => FALSE, 'msg' => '操作银行卡参数丢失，请重试！');
        }

        $card_exit = $this->find($card_id);

        if (empty($card_exit)) {

            return array('status' => FALSE, 'msg' => '操作的银行卡不存在！');
        }

        switch ($type) {

            case 'del':

                $row = $this->delete($card_id);

                $str = '您的卡号为:' . $card_exit['account_bank'] . '已删除！';

                break;
            case 'setdefault':

                //取消其他的默认
                $cancel_default = $this->where('user_id =' . $card_exit['user_id'])->setField('is_default', 0);

                $row = $this->where('id=' . $card_id)->setField('is_default', 1);
                
                $str = '默认银行卡设置成功！';
                
                break;
        }

        if ($row) {

            return array('status' => TRUE, 'msg' => $str);
        }

        return array('status' => FALSE, 'msg' => '操作失败，请重试!');
    }

}
