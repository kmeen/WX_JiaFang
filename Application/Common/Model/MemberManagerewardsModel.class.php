<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 执行董事分红
 */
class MemberManagerewardsModel extends Model {

    /*
     * 会员团队分红
     * 
     * &type 0 已发放 1、未发放
     */

    public function manage_teamdividend($user_id) {

        $map['user_id'] = $user_id;

        $teamdividend_list = $this->where($map)->order('create_time desc')->select();

        $total_teamdividend = $this->where($map)->sum('money');

        $total_teamdividend = $total_teamdividend ? $total_teamdividend : 0.00;

        return array('status' => 1, 'msg' => array('list' => $teamdividend_list, 'num' => $total_teamdividend));
    }
  
    /*
    *  获取执行董事下面的团队
     */
    public function get_manage_team($user_id) {
     
       $user_info  = D('Member')->where('id='.$user_id)->field('lft, rgt,flag')->find();
 
       $map['lft'] = array('gt', $user_info['lft']);
       $map['rgt']=array('lt', $user_info['rgt']);
       $map['flag']=$user_info['flag'];
       $member_list = D('Member')->where($map)->select();
  
       return $member_list ;
    }
    
}
