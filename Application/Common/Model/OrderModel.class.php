<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 订单模型
 * @author xzake
 */
class OrderModel extends Model {

    private $pay_name = array('0' => '', '1' => '微信支付', '2' => '积分支付');
    private $sum_field = array('0' => 'total', '1' => 'total_pv');

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
        array('name', 'require', '收货人不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('phone', 'require', '收货人手机不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('province', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('city', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('area', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('address', 'require', '收货地址不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
    );

    /*
     * 获取订单字段内容
     * 
     * @param item 可以是字符串，可以是数组
     */

    public function get_order_item($id, $item) {

        if (is_array($item)) {

            $item = implode(',', $item);

            return $data = $this->Field($item)->find($id);
        }

        return $data = $this->Field($item)->find($id);
    }

    /*
     * 会员订单支付金额计算
     * 
     * @param user_id string 用户id
     * 
     * @param string status array('0'=> '所有金额','1'=>'微信支付', '2'=>'积分支付');
     * 
     * @param string type array('0'=> 'total', '1'=>'pv', '2'=>)
     * 
     * @return status 处理状态 msg 状态信息
     * 
     */

    public function get_member_order_amount($user_id, $status = 0, $type = 0) {

        if (empty($user_id)) {

            return array('status' => FALSE, 'msg' => '参数丢失，请重试！');
        }

        $pay_name = $status ? $this->pay_name[$status] : '';  //支付类型：微信支付 积分支付 等

        $sum_field = $this->sum_field[$type];  //求和字段


        $map['user_id'] = $user_id;

        if (!empty($sum_field)) {

            $map['pay_name'] = $pay_name;
        }

        $map['pay_status'] = 2;  //支付订单

        $order_list = $this->where($map)->select();

        $amount = $this->where($map)->sum($sum_field);

        $amount = $amount ? $amount : 0.00;

        return array('status' => 1, 'msg' => array('list' => $order_list, 'num' => $amount));
    }

    /*
     * 统计平台总收益
     * 
     * @param  $array_time  时间范围
     */

    public function get_order_amount($array_time, $status = 0, $type = 0) {

        if (!empty($array_time)) {

            $map['create_time'] = array('between', array($array_time['min'], $array_time['max']));
        }

        $pay_name = $status ? $this->pay_name[$status] : '';  //支付类型： 所有 微信支付 积分支付 等

        $sum_field = $this->sum_field[$type];  //求和字段

        if (!empty($sum_field)) {

            $map['pay_name'] = $pay_name;
        }

        $map['pay_status'] = 2;

        $map['status'] = 1;

        $today_income = $this->where($map)->sum($sum_field);

        $today_income = $today_income ? $today_income : 0.00;

        return array('status' => 1, 'msg' => $today_income);
    }

    /*
     * 会员团队订单支付金额计算
     * 
     * @param array members_id 团队会员id数组
     *
     * @param array time_limit 时间段数组 字段值 min, max   
     * 
     * @param string status array('0'=> '所有金额','1'=>'微信支付', '2'=>'积分支付');
     * 
     * @param string type array('0'=> 'total', '1'=>'pv', '2'=>)
     * 
     * @return status 处理状态 msg 状态信息
     * 
     */

    public function get_membersteam_order_amount($members_id, $time_limit = null, $status = 0, $type = 0) {

        if (empty($members_id)) {

            return array('status' => FALSE, 'msg' => '参数丢失，请重试！');
        }

      //  $pay_name = $status ? $this->pay_name[$status] : '';  //支付类型：微信支付 积分支付 等

        $sum_field = $this->sum_field[$type];  //求和字段

        $pay_name='微信支付';
        
        $map['user_id'] = array('in', $members_id);

        if (!empty($sum_field)) {

            $map['pay_name'] = $pay_name;
        }

        //时间段
        if ($time_limit) {

            $map['pay_time'] = array('between', array($time_limit['min'], $time_limit['max']));
        }

        $map['pay_status'] = 2;  //支付订单

        $order_list = $this->where($map)->select();

        $amount = $this->where($map)->sum($sum_field);

        $amount = $amount ? $amount : 0.00;

        return array('status' => 1, 'msg' => array('list' => $order_list, 'num' => $amount));
    }

    /*
     * 获取会员团队共创额
     * 
     * @param array member_id 会员id
     * 
     * @param array time_limit 时间范围限制 默认所有订单
     */

    public function get_memberteam_create($member_id, $time_limit = null) {

        //获取会员下级团队信息
        $array_members_teams = D('Member')->members_team($member_id, 999, TRUE);

        if (!$array_members_teams['status']) {

            return $array_members_teams;
        }

        $members_team = $array_members_teams['msg'];


        if (empty($members_team)) {

            return array('status' => FALSE, 'msg' => '团队会员为空');
        }

        //获取会员团队的所有id
        $team_ids = array();

        foreach ($members_team as $v) {

            $team_ids[] = $v['id'];
        }


        $array_team_create = $this->get_membersteam_order_amount($team_ids, $time_limit, 1, 1);

        return $array_team_create;
    }

}
