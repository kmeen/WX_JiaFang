<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 会员团队分红模型
 * @author xingji
 */
class MemberTeamdividendModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('title', 'require', '名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('title', '1,100', '名称长度为1-100个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
//        array('title', 'checkTitle', '名称已经存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
//        array('create_time', NOW_TIME, self::MODEL_INSERT),
//        array('update_time', NOW_TIME, self::MODEL_BOTH),
//        array('status', '0', self::MODEL_INSERT),
    );

    /*
     * 会员团队分红
     * 
     * &type 0 已发放 1、未发放
     */

    public function member_teamdividend($user_id, $type = 0) {

        $map['user_id'] = $user_id;

        switch ($type) {

            case 1:
                $map['status'] = 0;

                break;
            default:

                $map['status'] = 1;

                break;
        }

        $teamdividend_list = $this->where($map)->order('create_time desc')->select();

        $total_teamdividend = $this->where($map)->sum('money');

        $total_teamdividend = $total_teamdividend ? $total_teamdividend : 0.00;

        return array('status' => 1, 'msg' => array('list' => $teamdividend_list, 'num' => $total_teamdividend));
    }

    /*
     * 添加团队共创分红数据
     * 
     * @param array $member_info （level 存储着会员的等级信息）单个会员信息
     * 
     * @param float team_create  会员团队共创
     * 
     * @param float scale  团队分红比例
     * 
     * @param int  $samelevel_num 个人团队下面同等级会员数
     * 
     * @param json $info  同等级会员信息 
     * 
     */

    public function add_memberteam_rewards($member_info, $team_create, $samelevel_num = 0, $same_members_info) {

        if (empty($member_info) || ($member_info['level_scale'] == 0)) {

            return array('status' => -1, 'msg' => '参数丢失，请重试！');
        }

        $total_team_create_dividend_rewards = $team_create * $member_info['level_scale'] / 100;

        $members_rewards = $total_team_create_dividend_rewards;

        if ($samelevel_num) {

            $members_rewards = $total_team_create_dividend_rewards / ($samelevel_num + 1);  //加1包括用户自己平分团队共创
        }

        $team_dividend = array(
            'user_id' => $member_info['id'],
            'dividend_no' => '',
            'total' => $team_create,
            'money' => $members_rewards,
            'level_title' => $member_info['level_title'],
            'level_scale' => $member_info['level_scale'],
            'same_level_member' => count($same_members_info),
            'samelevel_member_info' => json_encode($same_members_info),
            'create_time' => NOW_TIME,
            'status' => 1
        );

        $row = $this->add($team_dividend);

        if ($row) {

            return array('status' => 1, 'msg' => '会员-' . $member_info['id'] . '获得团队分红奖励:' . $members_rewards . '当前时间:' . date('Y-m-d H:i', time()));
        }

        return array('status' => -1, 'msg' => '会员' . $member_info['id'] . '团队分成添加失败！' . json_decode($team_dividend) . '当前时间:' . date('Y-m-d H:i', time()));
    }

}
