<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 会员分销奖励模型
 * @author xzake
 */
class MemberCommissionModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('title', 'require', '名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('title', '1,100', '名称长度为1-100个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
//        array('title', 'checkTitle', '名称已经存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
//        array('create_time', NOW_TIME, self::MODEL_INSERT),
//        array('update_time', NOW_TIME, self::MODEL_BOTH),
//        array('status', '0', self::MODEL_INSERT),
    );

    /*
     * 会员分销奖励
     * 
     * &type 0转赠成功 1、转赠失败
     */

    public function member_commission($user_id, $type = 0) {

        $map['user_id'] = $user_id;

        switch ($type) {

            case 1:
                $map['status'] = 0;

                break;
            default:
                
                $map['status'] = 1;

                break;
        }

        $commission_list = $this->where($map)->order('create_time desc')->select();

        $total_commission = $this->where($map)->sum('commission');

        $total_commission = $total_commission ? $total_commission : 0.00;

        return array('status' => 1, 'msg' => array('list' => $commission_list, 'num' => $total_commission));
    }
    
}
