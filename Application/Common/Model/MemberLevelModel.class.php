<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 分类模型
 * @author xzake
 */
class MemberLevelModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('title', 'require', '名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('title', '1,100', '名称长度为1-100个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
//        array('title', 'checkTitle', '名称已经存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
    );

    /*
     * 会员当前等级
     * 
     * @return  level 等级昵称 scale 参与分红比例 status TRUE参与分红 FALSE pass out
     */

    public function member_level($user_id) {

        $array_team_info = D('Member')->members_team($user_id, 1);

        if (!$array_team_info['status']) {

            return $array_team_info;
        }

        $team_num = count($array_team_info['msg']);

        #获取等级信息

        $level_list = $this->level_list();

        if (!$level_list['status']) {

            return $level_list;
        }

        $member = array('level' => '普通会员', 'scale' => 0, 'status' => FALSE);

        foreach ($level_list['msg'] as $v) {

            $limit_min = $v['limit']['min'];
            $limit_max = $v['limit']['max'];

            if (($team_num >= $limit_min) && ($team_num < $limit_max)) {

                $member['level'] = $v['title'];
                $member['scale'] = $v['scale'];
                $member['status'] = TRUE;
            }
        }

        return array('status' => 1, 'msg' => $member);
    }

    /*
     * 当前等级列表
     * 
     * @return status 处理状态 msg 会员等级数组
     */

    public function level_list() {

        $level_list = $this->order('id desc')->select();

        foreach ($level_list as $k => $v) {

            $data[$k] = array(
                'title' => $v['title'],
                'scale' => $v['scale'],
                'limit' => array(
                    'min' => $v['recommended_number_min'],
                    'max' => $v['recommended_number_max'],
                )
            );
        }

        if (empty($data)) {

            return array('status' => FALSE, 'msg' => '等级信息不存在');
        }

        return array('status' => 1, 'msg' => $data);
    }

}
