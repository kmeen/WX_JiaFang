<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 分类模型
 * @author xzake
 */
class MemberCashModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('title', 'require', '名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('title', '1,100', '名称长度为1-100个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
//        array('title', 'checkTitle', '名称已经存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '0', self::MODEL_INSERT),
    );

    /*
     * 会员提现金额 
     * 
     * &type 0所有提现状态   1、已审核 2、待审核提现 
     */

    public function member_cash($user_id, $type = 0) {

        $map['user_id'] = $user_id;

        switch ($type) {

            case 1:
                $map['status'] = 1;

                break;
            case 2:
                $map['status'] = 0;

                break;
            default:

                break;
        }

        $cash_list = $this->where($map)->order('create_time desc')->select();

        $total_cash = $this->where($map)->sum('total');

        $total_cash = $total_cash ? $total_cash : 0.00;

        return array('status' => 1, 'msg' => array('list' => $cash_list, 'num' => $total_cash));
    }

}
