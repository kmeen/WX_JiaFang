<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 分类模型
 * @author xzake
 */
class MemberFullbackModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('title', 'require', '名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
//        array('title', '1,100', '名称长度为1-100个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_BOTH),
//        array('title', 'checkTitle', '名称已经存在', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
//        array('create_time', NOW_TIME, self::MODEL_INSERT),
//        array('update_time', NOW_TIME, self::MODEL_BOTH),
//        array('status', '0', self::MODEL_INSERT),
    );

    /*
     * 会员全返信息
     * 
     * @type 0 success  1 false
     * 
     * @return status 执行状态  list全返列表信息 num 全返金额
     */

    public function member_fullback($user_id, $type = 0) {

        $map['user_id'] = $user_id;

        switch ($type) {

            case 1:
                $map['status'] = 0;

                break;
            default:

                $map['status'] = 1;

                break;
        }

        $fullback_list = $this->where($map)->order('create_time desc')->select();

        $total_fullback = $this->where($map)->sum('money'); //total 统计发放总额  total_face 实际发放总额

        $total_fullback = $total_fullback ? $total_fullback : 0.00;

        return array('status' => 1, 'msg' => array('list' => $fullback_list, 'num' => $total_fullback));
    }

    /*
     * 添加会员全返数据
     * 
     * @param array members_info 全返会员列表
     * @param float $reward      单个会员全返奖励
     * @param int $fullback_type 0 手动 1、自动
     * @param float total 平台总金额
     * @param array total
     * 
     * 
     */

    public function add_members_fullback($members_info, $reward = 0.00, $fullback_type = 0, $total = 0.00, $scale = 0) {

        $num = count($members_info);

        $time = NOW_TIME;

        $fullback_total = $total * $scale / 100;

        $status = 1;

        if (!$fullback_type) {

            $status = 0;
        }

        foreach ($members_info as $k => $v) {

            $fullback_data[] = array(
                'user_id' => $v['id'],
                'platform_income' => $total,
                'total' => $fullback_total,
                'scale' => $scale,
                'total_fact' => $fullback_total,
                'money' => $reward,
                'money_real' => $reward,
                'member_number' => $num,
                'create_time' => $time,
                'status' => $status
            );
        }

        if (empty($fullback_data)) {

            return array('status' => FALSE, 'msg' => date('Y-m-d H:i', time()) . '全返数据创建失败！');
        }


        $result = $this->addAll($fullback_data);

        if ($result) {

            return array('status' => 1, 'msg' => date('Y-m-d H:i', time()) . '今日全返已发放,' . '会员数:' . $num . '--全返总金额:' . $fullback_total . '--平台总收入:' . $total . '--今日分成比例:' . $scale);
        } else {

            return array('status' => FALSE, 'msg' => date('Y-m-d H:i', time()) . '全返数据添加失败！');
        }
    }

}
