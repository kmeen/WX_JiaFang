<?php

// +----------------------------------------------------------------------
// | 分销管家
// +----------------------------------------------------------------------
// | Copyright (c) 2015 http://www.kmeen.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: xzake <http://www.kmeen.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;
use Think\Log;

/**
 * 会员公排模型
 * @author xzake
 */
class GongpaiModel extends Model {

    /**
     * 自动验证规则
     * @author xzake
     */
    protected $_validate = array(
//        array('name', 'require', '真实姓名不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_UPDATE),
//        array('nickname', '1,30', '真实姓名长度为1-30个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_UPDATE),
//        //验证邮箱
//        array('email', 'email', '邮箱格式不正确', self::EXISTS_VALIDATE, 'regex', self::MODEL_UPDATE),
//        array('email', '1,32', '邮箱长度为1-32个字符', self::EXISTS_VALIDATE, 'length', self::MODEL_UPDATE),
//        array('email', '', '邮箱被占用', self::EXISTS_VALIDATE, 'unique', self::MODEL_UPDATE),
//        //验证手机号码
//        array('phone', '/^1\d{10}$/', '手机号码格式不正确', self::EXISTS_VALIDATE, 'regex', self::MODEL_UPDATE),
//        array('phone', '', '手机号被占用', self::EXISTS_VALIDATE, 'unique', self::MODEL_UPDATE),
    );

    /**
     * 自动完成规则
     * @author xzake
     */
    protected $_auto = array(
//        array('create_time', NOW_TIME, self::MODEL_INSERT),
//        array('update_time', NOW_TIME, self::MODEL_BOTH),
//        array('status', '1', self::MODEL_INSERT),
    );

    /*
     * 公排会员的奖金
     * 
     * @param int user_id 会员id
     * 
     * @param int limit_level  公排限制层数
     * 
     * @param int single_reward 见点奖 
     * 
     * @return array status 处理状态  msg正确(奖励金额) 错误(错误提示)
     */

    public function gp_reward($user_id, $limit_level = 0, $single_reward = 0) {

        if (empty($user_id)) {

            return array('status' => -1, 'msg' => '参数错误');
        }

        if ($limit_level <= 0) {

            return array('status' => -1, 'msg' => '奖励层数为0或小于0');
        }

        if ($single_reward <= 0) {

            return array('status' => -1, 'msg' => '奖励金额小于等于0');
        }
        #计算会员下面有多少人
        $gp_member = $this->where('user_id =' . $user_id)->find();

        #计算公排会员限制级别内的会员数
        $gp_map['lft'] = array('gt', $gp_member['lft']);
        $gp_map['rgt'] = array('lt', $gp_member['rgt']);
        $gp_map['lvl'] = array('between', array($gp_member['lvl'], $gp_member['lvl'] + $limit_level));

        $count = $this->where($gp_map)->count();

        $gp_reward = $count * $single_reward;

        $gp_reward = $gp_reward ? $gp_reward : '0.00';

        return array('status' => 1, 'msg' => $gp_reward);
    }

    /*
     * 统计公排下级会员信息
     * &gp 公排
     * 
     * &type  0 获取团队下的
     */

    public function gp_team_info($user_id, $type = 0) {

        exit('wait');
    }

}
