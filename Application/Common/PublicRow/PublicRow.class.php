<?php

namespace Common\PublicRow;

use Think\Log;

class PublicRow {

    protected $openAutoArrange = TRUE;
    protected $maxChildNodeNum = 2;

    public function addNode($data) {

        Log::write('公排开始:' . json_encode($data));

        if (empty($data)) {

            return array('status' => false, 'msg' => '参数丢失！');
        }

        if (!empty(M('Gongpai')->where(array('user_id' => $data['user_id']))->find())) {

            return array('status' => false, 'msg' => '用户' . $data['user_id'] . '已经在公排队列中，不能再次添加！');
        }

        return $this->insertNode($data);
    }

    private function insertNode($data) {

        Log::write('开始公排排列');

        $model = D('Gongpai');

        if ($this->openAutoArrange && $this->maxChildNodeNum > 0) {

            $parentItem = $model->where("(rgt - lft - 1 ) / 2  < {$this->maxChildNodeNum}")
                    ->order('id asc')
                    ->find();
        } else if (isset($data['pid'])) {

            $parentItem = $model->find($data['pid']);
        }

        Log::write('parentItem:' . json_encode($parentItem));

        if (empty($parentItem)) {
            Log::write('first member');
            $data['lft'] = 1;

            $data['rgt'] = 2;

            #这里是添加用户 而不是更新信息
            $insertId = $model->add($data);

//            Log::write('first member sql:' . $model->getLastSql());

            Log::write('first member id:' . $insertId);

            if ($insertId) {

                $fmap['id'] = array('eq', $insertId);

                $fdata = array(
                    'flag' => $insertId,
                    'create_time' => NOW_TIME,
                );

                $model->where($fmap)->save($fdata);

                M('Member')->where(array('id' => $data['user_id']))->setField('is_publicrow_member', 1); //更改会员表中 公排会员字段值

                return array('status' => 1, 'msg' => '用户:' . $data['user_id'] . '加入了公排队列，他的序号为' . $insertId);
            }
        } else {

            $data['lft'] = $parentItem['rgt'];

            $data['rgt'] = $data['lft'] + 1;

            $data['lvl'] = $parentItem['lvl'] + 1;

            $data['flag'] = $parentItem['flag'];

            $data['pid'] = $parentItem['id'];


            $data['create_time'] = NOW_TIME;

            $data['update_time'] = NOW_TIME;
            #先添加用户
            $insertId = $model->add($data);

            Log::write('team member id:' . $insertId);

            if ($insertId) {

                $rmap['rgt'] = array('egt', $parentItem['rgt']);

                $rmap['flag'] = array('eq', $parentItem['flag']);

                $rmap['id'] = array('neq', $insertId);

                $model->where($rmap)->setInc('rgt', 2);   //更新团队会员右值

                $lmap['lft'] = array('egt', $parentItem['rgt']);

                $lmap['flag'] = array('eq', $parentItem['flag']);

                $lmap['id'] = array('neq', $insertId);

                $model->where($lmap)->setInc('lft', 2);   //更新团队会员左值

                M('Member')->where(array('id' => $data['user_id']))->setField('is_publicrow_member', 1); //更改会员表中 公排会员字段值

                return array('status' => 1, 'msg' => '用户:' . $data['user_id'] . '加入了指定推荐人下面的公排队列，他的序号为' . $insertId);
            }
        }

        return array('status' => false, 'msg' => '进入公排失败结果:' . $data['user_id'] . '加入公排队列失败！');
    }

}
