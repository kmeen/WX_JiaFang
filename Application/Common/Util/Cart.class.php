<?php

namespace Common\Util;

use Common\Model\MemberModel;

class Cart {

    public function __cunstruct() {

        if (!isset($_SESSION['cart'])) {

            $_SESSION['cart'] = array();
        }
    }

    /*
      添加商品
      param int $id 商品主键
      string $name 商品名称
      float $price 商品价格
      int $num 购物数量
     */

//    public function addItem($id, $name, $price, $num, $img, $original_price = 0) {
//        //如果该商品已存在则直接加其数量
//        if (isset($_SESSION['cart'][$id])) {
//
//            $this->incNum($id, $num);
//
//            return;
//        }
//
//        $item = array();
//
//        $item['id'] = $id;
//
//        $item['name'] = $name;
//
//        $item['price'] = $price;
//
//        $item['original_price'] = $original_price;
//
//        $item['num'] = $num;
//
//        $item['img'] = $img;
//
//        $_SESSION['cart'][$id] = $item;
//    }

    /*
      添加商品
      param int $id 商品主键
      string $name 商品名称
      float $price 商品价格
      int $num 购物数量
     */

    public function addGoods($id, $num = 1) {
                
        if (empty($id)) {

            return FALSE;
        }
        
 //       $user_model = new MemberModel();
        
        //如果该商品已存在则直接加其数量
        if (isset($_SESSION['cart'][$id])) {
            $this->incNum($id, $num);
            return array('status' => 1, 'msg' => '成功加入购物车');
        }

        $goods = D("Goods")->find($id);

        if (empty($goods)) {
            return array('status' => -1, 'msg' => '商品不存在');
        }

        $goods['price'] = D('member')->getPrice($goods['price'], $_SESSION['user_id']);
        
   //    $goods['price'] =  $_SESSION['manage_price'][$id];

        $item = array(
            'id' => $goods['id'],
            'name' => $goods['title'],
            'price' => $goods['price'],
            'original_price' => $goods['original_price'],
            'num' => $num,
            'img' => get_cover($goods['cover']),
            'pv' => $goods['pv']
        );

        $_SESSION['cart'][$id] = $item;

        return array('status' => 1, 'msg' => '成功加入购物车');
    }

    /*
      修改购物车中的商品数量
      int $id 商品主键
      int $num 某商品修改后的数量，即直接把某商品
      的数量改为$num
     */

    public function modNum($id, $num = 1) {

        if (!isset($_SESSION['cart'][$id])) {

            return FALSE;
        }

        $_SESSION['cart'][$id]['num'] = $num;

        return TRUE;
    }

    /*
      商品数量+1
     */

    public function incNum($id, $num = 1) {

        if (isset($_SESSION['cart'][$id])) {

            $_SESSION['cart'][$id]['num'] += $num;
        }
    }

    /*
      商品数量-1
     */

    public function decNum($id, $num = 1) {

        if (isset($_SESSION['cart'][$id])) {

            $_SESSION['cart'][$id]['num'] -= $num;
        }

        //如果减少后，数量为0，则把这个商品删掉
        if ($_SESSION['cart'][$id]['num'] < 1) {

            $this->delItem($id);
        }
    }

    /*
      删除商品
     */

    public function delItem($id) {

        unset($_SESSION['cart'][$id]);
    }

    /*
      获取单个商品
     */

    public function getItem($id) {

        return $_SESSION['cart'][$id];
    }

    /*
      查询购物车中商品的种类
     */

    public function getCnt() {

        return count($_SESSION['cart']);
    }

    /*
      查询购物车中商品的个数
     */

    public function getNum() {

        if ($this->getCnt() == 0) {
            //种数为0，个数也为0
            return 0;
        }

        $sum = 0;

        $data = $_SESSION['cart'];

        foreach ($data as $item) {

            $sum += $item['num'];
        }

        return $sum;
    }
    
    
     /*
      购物车中商品的总金额
     * @param 获取购物车字段值之和 默认为price (同时可以传入pv值 或者 commission)
      * @xingji modify
     */

    public function getPrice($param = 'price') {

        //数量为0，价钱为0

        if ($this->getCnt() == 0) {

            return 0;
        }

        $total = 0.00;

        $data = $_SESSION['cart'];

        foreach ($data as $item) {

            $total += $item['num'] * $item[$param];
        }

        return sprintf("%01.2f", $total);
    }
    

//    /*
//      购物车中商品的总金额
//     * @param 获取购物车字段值之和
//     */
//
//    public function getPrice() {
//
//        //数量为0，价钱为0
//
//        if ($this->getCnt() == 0) {
//
//            return 0;
//        }
//
//        $price = 0.00;
//
//        $data = $_SESSION['cart'];
//
//        foreach ($data as $item) {
//
//            $price += $item['num'] * $item['price'];
//        }
//
//        return sprintf("%01.2f", $price);
//    }

    /*
      清空购物车
     */

    public function clear() {

        $_SESSION['cart'] = array();
    }

    /*
      清空购物车
     */

    public function getCart() {

        return $_SESSION['cart'];
    }

}
