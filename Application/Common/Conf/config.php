<?php

define('WEB_HOST', 'http://fx.kmeen.com');
return array(
    //数据库配置
    'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => '127.0.0.1', // 服务器地址
    'DB_NAME' => 'xixijiafang', // 数据库名
    'DB_USER' => 'root', // 用户名
    //'DB_PWD' => 'root', // 密码
    'DB_PWD' => 'ddm@126.com', // 密码
    'DB_PORT' => '3306', // 端口
    'DB_PREFIX' => 'kmeen_', // 数据库表前缀
    //URL模式
    'URL_MODEL' => '3',
    //全局过滤配置
    'DEFAULT_FILTER' => '', //默认为htmlspecialchars
    //URL配置
    'URL_CASE_INSENSITIVE' => true, //不区分大小写
    //应用配置
    'DEFAULT_MODULE' => 'Manage',
    'MODULE_DENY_LIST' => array('Common'),
    'MODULE_ALLOW_LIST' => array('Manage', 'WeChat'),
    //预先加载的标签库
    //'TAGLIB_PRE_LOAD' => 'WeChat\\TagLib\\Kmeen',
    //表单类型
    'FORM_ITEM_TYPE' => array(
        'hidden' => '隐藏',
        'num' => '数字',
        'text' => '字符串',
        'textarea' => '文本',
        'array' => '数组',
        'password' => '密码',
        'radio' => '单选按钮',
        'checkbox' => '复选框',
        'select' => '下拉框',
        'icon' => '图标',
        'date  ' => '日期',
        'datetime' => '时间',
        'picture' => '图片',
        'kindeditor' => '编辑器',
        'tags' => '标签',
        'board  ' => '拖动排序',
    ),
    //文件上传相关配置
    'UPLOAD_CONFIG' => array(
        'mimes' => '', //允许上传的文件MiMe类型
        'maxSize' => 2 * 1024 * 1024, //上传的文件大小限制 (0-不做限制，默认为2M，后台配置会覆盖此值)
        'autoSub' => true, //自动子目录保存文件
        'subName' => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => './Uploads/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid', ''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt' => '', //文件保存后缀，空则使用原后缀
        'replace' => false, //存在同名是否覆盖
        'hash' => true, //是否生成hash编码
        'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ),
    'WECHAT_TYPE_LIST' => array(
        0 => '普通订阅号',
        1 => '认证订阅号/普通服务号',
        2 => '认证服务号'
    ),
    'SHOW_SELECT_OPTION' => array(
        0 => '不显示',
        1 => '显示',
    ),
    'WECHAT_MENU_EVENT_LIST_TITLE' => array(
        '点击推事件',
        '跳转URL',
        '扫码推事件',
        '扫码带提示',
        '弹出系统拍照发图',
        '弹出拍照或者相册发图',
        '弹出微信相册发图器',
        '弹出地理位置选择器',
        '无事件的一级菜单'
    ),
    'WECHAT_MENU_EVENT_LIST' => array(
        "click",
        "view",
        "scancode_push",
        "scancode_waitmsg",
        "pic_sysphoto",
        "pic_photo_or_album",
        "pic_weixin",
        "location_select",
        "none"
    ),
    'WECHAT_MENU_EVENT_LIST_ALL' => array(
        "click" => '点击推事件',
        "view" => '跳转URL',
        "scancode_push" => '扫码推事件',
        "scancode_waitmsg" => '扫码带提示',
        "pic_sysphoto" => '弹出系统拍照发图',
        "pic_photo_or_album" => '弹出拍照或者相册发图',
        "pic_weixin" => '弹出微信相册发图器',
        "location_select" => '弹出地理位置选择器',
        "none" => '无事件的一级菜单'
    ),
    /* 微信支付配置 */
    'WxPayConf_pub' => array(
        'APPID' => 'wx762b752f78940dbb',
        'MCHID' => '1250166801',
        'KEY' => 'b5IpyvHEaAeR9oZOQEKLbOka0WLuRw2M',
        'APPSECRET' => 'df8c8cf62414bb5a54a29516834d44cf',
        'JS_API_CALL_URL' => WEB_HOST . '/index.php/WeChat/Paya/jsApiCall.html',
        'SSLCERT_PATH' => WEB_HOST . '/ThinkPHP/Library/Vendor/WxPayPubHelper/cert/apiclient_cert.pem',
        'SSLKEY_PATH' => WEB_HOST . '/ThinkPHP/Library/Vendor/WxPayPubHelper/cert/apiclient_key.pem',
        'NOTIFY_URL' => WEB_HOST . '/index.php/WeChat/Paya/notify',
        'CURL_TIMEOUT' => 30
    ),
    /* 分类图标 */
    'Category_icon' => array(
//        '0' => '无',
        '1' => '运动',
        '2' => '电器',
        '3' => '配饰',
        '4' => '孕婴',
        '5' => '箱包',
        '6' => '服饰',
        '7' => '数码',
        '8' => '家纺',
        '9' => '美护',
    ),
);
